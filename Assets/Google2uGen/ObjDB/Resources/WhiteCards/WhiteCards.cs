//----------------------------------------------
//    Google2u: Google Doc Unity integration
//         Copyright © 2015 Litteratus
//
//        This file has been auto-generated
//              Do not manually edit
//----------------------------------------------

using UnityEngine;
using System.Globalization;

namespace Google2u
{
	[System.Serializable]
	public class WhiteCardsRow : IGoogle2uRow
	{
		public string _NAME;
		public string _COLLECTION;
		public WhiteCardsRow(string __po, string __NAME, string __COLLECTION) 
		{
			_NAME = __NAME.Trim();
			_COLLECTION = __COLLECTION.Trim();
		}

		public int Length { get { return 2; } }

		public string this[int i]
		{
		    get
		    {
		        return GetStringDataByIndex(i);
		    }
		}

		public string GetStringDataByIndex( int index )
		{
			string ret = System.String.Empty;
			switch( index )
			{
				case 0:
					ret = _NAME.ToString();
					break;
				case 1:
					ret = _COLLECTION.ToString();
					break;
			}

			return ret;
		}

		public string GetStringData( string colID )
		{
			var ret = System.String.Empty;
			switch( colID )
			{
				case "_NAME":
					ret = _NAME.ToString();
					break;
				case "_COLLECTION":
					ret = _COLLECTION.ToString();
					break;
			}

			return ret;
		}
		public override string ToString()
		{
			string ret = System.String.Empty;
			ret += "{" + "_NAME" + " : " + _NAME.ToString() + "} ";
			ret += "{" + "_COLLECTION" + " : " + _COLLECTION.ToString() + "} ";
			return ret;
		}
	}
	public class WhiteCards :  Google2uComponentBase, IGoogle2uDB
	{
		public enum rowIds {
			CARD1, CARD2, CARD3, CARD4, CARD5, CARD6, CARD7, CARD8, CARD9, CARD10, CARD11, CARD12, CARD13, CARD14, CARD15, CARD16, CARD17, CARD18
			, CARD19, CARD20, CARD21, CARD22, CARD23, CARD24, CARD25, CARD26, CARD27, CARD28, CARD29, CARD30, CARD31, CARD32, CARD33, CARD34, CARD35, CARD36, CARD37, CARD38
			, CARD39, CARD40, CARD41, CARD42, CARD43, CARD44, CARD45, CARD46, CARD47, CARD48, CARD49, CARD50, CARD51, CARD52, CARD53, CARD54, CARD55, CARD56, CARD57, CARD58
			, CARD59, CARD60, CARD61, CARD62, CARD63, CARD64, CARD65, CARD66, CARD67, CARD68, CARD69, CARD70, CARD71, CARD72, CARD73, CARD74, CARD75, CARD76, CARD77, CARD78
			, CARD79, CARD80, CARD81, CARD82, CARD83, CARD84, CARD85, CARD86, CARD87, CARD88, CARD89, CARD90, CARD91, CARD92, CARD93, CARD94, CARD95, CARD96, CARD97, CARD98
			, CARD99, CARD100, CARD101, CARD102, CARD103, CARD104, CARD105, CARD106, CARD107, CARD108, CARD109, CARD110, CARD111, CARD112, CARD113, CARD114, CARD115, CARD116, CARD117, CARD118
			, CARD119, CARD120, CARD121, CARD122, CARD123, CARD124, CARD125, CARD126, CARD127, CARD128, CARD129, CARD130, CARD131, CARD132, CARD133, CARD134, CARD135, CARD136, CARD137, CARD138
			, CARD139, CARD140, CARD141, CARD142, CARD143, CARD144, CARD145, CARD146, CARD147, CARD148, CARD149, CARD150, CARD151, CARD152, CARD153, CARD154, CARD155, CARD156, CARD157, CARD158
			, CARD159, CARD160, CARD161, CARD162, CARD163, CARD164, CARD165, CARD166, CARD167, CARD168, CARD169, CARD170, CARD171, CARD172, CARD173, CARD174, CARD175, CARD176, CARD177, CARD178
			, CARD179, CARD180, CARD181, CARD182, CARD183, CARD184, CARD185, CARD186, CARD187, CARD188, CARD189, CARD190, CARD191, CARD192, CARD193, CARD194, CARD195, CARD196, CARD197, CARD198
			, CARD199, CARD200, CARD201, CARD202, CARD203, CARD204, CARD205, CARD206, CARD207, CARD208, CARD209, CARD210, CARD211, CARD212, CARD213, CARD214, CARD215, CARD216, CARD217, CARD218
			, CARD219, CARD220, CARD221, CARD222, CARD223, CARD224, CARD225, CARD226, CARD227, CARD228, CARD229, CARD230, CARD231, CARD232, CARD233, CARD234, CARD235, CARD236, CARD237, CARD238
			, CARD239, CARD240, CARD241, CARD242, CARD243, CARD244, CARD245, CARD246, CARD247, CARD248, CARD249, CARD250, CARD251, CARD252, CARD253, CARD254, CARD255, CARD256, CARD257, CARD258
			, CARD259, CARD260, CARD261, CARD262, CARD263, CARD264, CARD265, CARD266, CARD267, CARD268, CARD269, CARD270, CARD271, CARD272, CARD273, CARD274, CARD275, CARD276, CARD277, CARD278
			, CARD279, CARD280, CARD281, CARD282, CARD283, CARD284, CARD285, CARD286, CARD287, CARD288, CARD289, CARD290, CARD291, CARD292, CARD293, CARD294, CARD295, CARD296, CARD297, CARD298
			, CARD299, CARD300, CARD301, CARD302, CARD303, CARD304, CARD305, CARD306, CARD307, CARD308, CARD309, CARD310, CARD311, CARD312, CARD313, CARD314, CARD315, CARD316, CARD317, CARD318
			, CARD319, CARD320, CARD321, CARD322, CARD323, CARD324
		};
		public string [] rowNames = {
			"CARD1", "CARD2", "CARD3", "CARD4", "CARD5", "CARD6", "CARD7", "CARD8", "CARD9", "CARD10", "CARD11", "CARD12", "CARD13", "CARD14", "CARD15", "CARD16", "CARD17", "CARD18"
			, "CARD19", "CARD20", "CARD21", "CARD22", "CARD23", "CARD24", "CARD25", "CARD26", "CARD27", "CARD28", "CARD29", "CARD30", "CARD31", "CARD32", "CARD33", "CARD34", "CARD35", "CARD36", "CARD37", "CARD38"
			, "CARD39", "CARD40", "CARD41", "CARD42", "CARD43", "CARD44", "CARD45", "CARD46", "CARD47", "CARD48", "CARD49", "CARD50", "CARD51", "CARD52", "CARD53", "CARD54", "CARD55", "CARD56", "CARD57", "CARD58"
			, "CARD59", "CARD60", "CARD61", "CARD62", "CARD63", "CARD64", "CARD65", "CARD66", "CARD67", "CARD68", "CARD69", "CARD70", "CARD71", "CARD72", "CARD73", "CARD74", "CARD75", "CARD76", "CARD77", "CARD78"
			, "CARD79", "CARD80", "CARD81", "CARD82", "CARD83", "CARD84", "CARD85", "CARD86", "CARD87", "CARD88", "CARD89", "CARD90", "CARD91", "CARD92", "CARD93", "CARD94", "CARD95", "CARD96", "CARD97", "CARD98"
			, "CARD99", "CARD100", "CARD101", "CARD102", "CARD103", "CARD104", "CARD105", "CARD106", "CARD107", "CARD108", "CARD109", "CARD110", "CARD111", "CARD112", "CARD113", "CARD114", "CARD115", "CARD116", "CARD117", "CARD118"
			, "CARD119", "CARD120", "CARD121", "CARD122", "CARD123", "CARD124", "CARD125", "CARD126", "CARD127", "CARD128", "CARD129", "CARD130", "CARD131", "CARD132", "CARD133", "CARD134", "CARD135", "CARD136", "CARD137", "CARD138"
			, "CARD139", "CARD140", "CARD141", "CARD142", "CARD143", "CARD144", "CARD145", "CARD146", "CARD147", "CARD148", "CARD149", "CARD150", "CARD151", "CARD152", "CARD153", "CARD154", "CARD155", "CARD156", "CARD157", "CARD158"
			, "CARD159", "CARD160", "CARD161", "CARD162", "CARD163", "CARD164", "CARD165", "CARD166", "CARD167", "CARD168", "CARD169", "CARD170", "CARD171", "CARD172", "CARD173", "CARD174", "CARD175", "CARD176", "CARD177", "CARD178"
			, "CARD179", "CARD180", "CARD181", "CARD182", "CARD183", "CARD184", "CARD185", "CARD186", "CARD187", "CARD188", "CARD189", "CARD190", "CARD191", "CARD192", "CARD193", "CARD194", "CARD195", "CARD196", "CARD197", "CARD198"
			, "CARD199", "CARD200", "CARD201", "CARD202", "CARD203", "CARD204", "CARD205", "CARD206", "CARD207", "CARD208", "CARD209", "CARD210", "CARD211", "CARD212", "CARD213", "CARD214", "CARD215", "CARD216", "CARD217", "CARD218"
			, "CARD219", "CARD220", "CARD221", "CARD222", "CARD223", "CARD224", "CARD225", "CARD226", "CARD227", "CARD228", "CARD229", "CARD230", "CARD231", "CARD232", "CARD233", "CARD234", "CARD235", "CARD236", "CARD237", "CARD238"
			, "CARD239", "CARD240", "CARD241", "CARD242", "CARD243", "CARD244", "CARD245", "CARD246", "CARD247", "CARD248", "CARD249", "CARD250", "CARD251", "CARD252", "CARD253", "CARD254", "CARD255", "CARD256", "CARD257", "CARD258"
			, "CARD259", "CARD260", "CARD261", "CARD262", "CARD263", "CARD264", "CARD265", "CARD266", "CARD267", "CARD268", "CARD269", "CARD270", "CARD271", "CARD272", "CARD273", "CARD274", "CARD275", "CARD276", "CARD277", "CARD278"
			, "CARD279", "CARD280", "CARD281", "CARD282", "CARD283", "CARD284", "CARD285", "CARD286", "CARD287", "CARD288", "CARD289", "CARD290", "CARD291", "CARD292", "CARD293", "CARD294", "CARD295", "CARD296", "CARD297", "CARD298"
			, "CARD299", "CARD300", "CARD301", "CARD302", "CARD303", "CARD304", "CARD305", "CARD306", "CARD307", "CARD308", "CARD309", "CARD310", "CARD311", "CARD312", "CARD313", "CARD314", "CARD315", "CARD316", "CARD317", "CARD318"
			, "CARD319", "CARD320", "CARD321", "CARD322", "CARD323", "CARD324"
		};
		public System.Collections.Generic.List<WhiteCardsRow> Rows = new System.Collections.Generic.List<WhiteCardsRow>();
		public override void AddRowGeneric (System.Collections.Generic.List<string> input)
		{
			Rows.Add(new WhiteCardsRow(input[0],input[1],input[2]));
		}
		public override void Clear ()
		{
			Rows.Clear();
		}
		public IGoogle2uRow GetGenRow(string in_RowString)
		{
			IGoogle2uRow ret = null;
			try
			{
				ret = Rows[(int)System.Enum.Parse(typeof(rowIds), in_RowString)];
			}
			catch(System.ArgumentException) {
				Debug.LogError( in_RowString + " is not a member of the rowIds enumeration.");
			}
			return ret;
		}
		public IGoogle2uRow GetGenRow(rowIds in_RowID)
		{
			IGoogle2uRow ret = null;
			try
			{
				ret = Rows[(int)in_RowID];
			}
			catch( System.Collections.Generic.KeyNotFoundException ex )
			{
				Debug.LogError( in_RowID + " not found: " + ex.Message );
			}
			return ret;
		}
		public WhiteCardsRow GetRow(rowIds in_RowID)
		{
			WhiteCardsRow ret = null;
			try
			{
				ret = Rows[(int)in_RowID];
			}
			catch( System.Collections.Generic.KeyNotFoundException ex )
			{
				Debug.LogError( in_RowID + " not found: " + ex.Message );
			}
			return ret;
		}
		public WhiteCardsRow GetRow(string in_RowString)
		{
			WhiteCardsRow ret = null;
			try
			{
				ret = Rows[(int)System.Enum.Parse(typeof(rowIds), in_RowString)];
			}
			catch(System.ArgumentException) {
				Debug.LogError( in_RowString + " is not a member of the rowIds enumeration.");
			}
			return ret;
		}

	}

}
