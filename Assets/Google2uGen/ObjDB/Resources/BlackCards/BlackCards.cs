//----------------------------------------------
//    Google2u: Google Doc Unity integration
//         Copyright © 2015 Litteratus
//
//        This file has been auto-generated
//              Do not manually edit
//----------------------------------------------

using UnityEngine;
using System.Globalization;

namespace Google2u
{
	[System.Serializable]
	public class BlackCardsRow : IGoogle2uRow
	{
		public string _DESCRIPTION;
		public string _COLLECTION;
		public string _GAPS;
		public BlackCardsRow(string __GOOGLEFU_ID, string __DESCRIPTION, string __COLLECTION, string __GAPS) 
		{
			_DESCRIPTION = __DESCRIPTION.Trim();
			_COLLECTION = __COLLECTION.Trim();
			_GAPS = __GAPS.Trim();
		}

		public int Length { get { return 3; } }

		public string this[int i]
		{
		    get
		    {
		        return GetStringDataByIndex(i);
		    }
		}

		public string GetStringDataByIndex( int index )
		{
			string ret = System.String.Empty;
			switch( index )
			{
				case 0:
					ret = _DESCRIPTION.ToString();
					break;
				case 1:
					ret = _COLLECTION.ToString();
					break;
				case 2:
					ret = _GAPS.ToString();
					break;
			}

			return ret;
		}

		public string GetStringData( string colID )
		{
			var ret = System.String.Empty;
			switch( colID )
			{
				case "_DESCRIPTION":
					ret = _DESCRIPTION.ToString();
					break;
				case "_COLLECTION":
					ret = _COLLECTION.ToString();
					break;
				case "_GAPS":
					ret = _GAPS.ToString();
					break;
			}

			return ret;
		}
		public override string ToString()
		{
			string ret = System.String.Empty;
			ret += "{" + "_DESCRIPTION" + " : " + _DESCRIPTION.ToString() + "} ";
			ret += "{" + "_COLLECTION" + " : " + _COLLECTION.ToString() + "} ";
			ret += "{" + "_GAPS" + " : " + _GAPS.ToString() + "} ";
			return ret;
		}
	}
	public class BlackCards :  Google2uComponentBase, IGoogle2uDB
	{
		public enum rowIds {
			CARD1, CARD2, CARD3, CARD4, CARD5, CARD6, CARD7, CARD8, CARD9, CARD10, CARD11, CARD12, CARD13, CARD14, CARD15, CARD16, CARD17, CARD18
			, CARD19, CARD20, CARD21, CARD22, CARD23, CARD24, CARD25, CARD26, CARD27, CARD28, CARD29, CARD30, CARD31, CARD32, CARD33, CARD34, CARD35, CARD36, CARD37, CARD38
			, CARD39, CARD40, CARD41, CARD42, CARD43, CARD44, CARD45, CARD46, CARD47, CARD48, CARD49, CARD50, CARD51, CARD52, CARD53, CARD54, CARD55, CARD56, CARD57, CARD58
			, CARD59, CARD60, CARD61, CARD62, CARD63, CARD64, CARD65, CARD66, CARD67, CARD68, CARD69, CARD70, CARD71, CARD72, CARD73, CARD74, CARD75, CARD76, CARD77, CARD78
			, CARD79, CARD80, CARD81, CARD82, CARD83, CARD84, CARD85, CARD86
		};
		public string [] rowNames = {
			"CARD1", "CARD2", "CARD3", "CARD4", "CARD5", "CARD6", "CARD7", "CARD8", "CARD9", "CARD10", "CARD11", "CARD12", "CARD13", "CARD14", "CARD15", "CARD16", "CARD17", "CARD18"
			, "CARD19", "CARD20", "CARD21", "CARD22", "CARD23", "CARD24", "CARD25", "CARD26", "CARD27", "CARD28", "CARD29", "CARD30", "CARD31", "CARD32", "CARD33", "CARD34", "CARD35", "CARD36", "CARD37", "CARD38"
			, "CARD39", "CARD40", "CARD41", "CARD42", "CARD43", "CARD44", "CARD45", "CARD46", "CARD47", "CARD48", "CARD49", "CARD50", "CARD51", "CARD52", "CARD53", "CARD54", "CARD55", "CARD56", "CARD57", "CARD58"
			, "CARD59", "CARD60", "CARD61", "CARD62", "CARD63", "CARD64", "CARD65", "CARD66", "CARD67", "CARD68", "CARD69", "CARD70", "CARD71", "CARD72", "CARD73", "CARD74", "CARD75", "CARD76", "CARD77", "CARD78"
			, "CARD79", "CARD80", "CARD81", "CARD82", "CARD83", "CARD84", "CARD85", "CARD86"
		};
		public System.Collections.Generic.List<BlackCardsRow> Rows = new System.Collections.Generic.List<BlackCardsRow>();
		public override void AddRowGeneric (System.Collections.Generic.List<string> input)
		{
			Rows.Add(new BlackCardsRow(input[0],input[1],input[2],input[3]));
		}
		public override void Clear ()
		{
			Rows.Clear();
		}
		public IGoogle2uRow GetGenRow(string in_RowString)
		{
			IGoogle2uRow ret = null;
			try
			{
				ret = Rows[(int)System.Enum.Parse(typeof(rowIds), in_RowString)];
			}
			catch(System.ArgumentException) {
				Debug.LogError( in_RowString + " is not a member of the rowIds enumeration.");
			}
			return ret;
		}
		public IGoogle2uRow GetGenRow(rowIds in_RowID)
		{
			IGoogle2uRow ret = null;
			try
			{
				ret = Rows[(int)in_RowID];
			}
			catch( System.Collections.Generic.KeyNotFoundException ex )
			{
				Debug.LogError( in_RowID + " not found: " + ex.Message );
			}
			return ret;
		}
		public BlackCardsRow GetRow(rowIds in_RowID)
		{
			BlackCardsRow ret = null;
			try
			{
				ret = Rows[(int)in_RowID];
			}
			catch( System.Collections.Generic.KeyNotFoundException ex )
			{
				Debug.LogError( in_RowID + " not found: " + ex.Message );
			}
			return ret;
		}
		public BlackCardsRow GetRow(string in_RowString)
		{
			BlackCardsRow ret = null;
			try
			{
				ret = Rows[(int)System.Enum.Parse(typeof(rowIds), in_RowString)];
			}
			catch(System.ArgumentException) {
				Debug.LogError( in_RowString + " is not a member of the rowIds enumeration.");
			}
			return ret;
		}

	}

}
