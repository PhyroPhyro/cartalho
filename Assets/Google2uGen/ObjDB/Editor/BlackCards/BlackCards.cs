using UnityEngine;
using UnityEditor;

namespace Google2u
{
	[CustomEditor(typeof(BlackCards))]
	public class BlackCardsEditor : Editor
	{
		public int Index = 0;
		public override void OnInspectorGUI ()
		{
			BlackCards s = target as BlackCards;
			BlackCardsRow r = s.Rows[ Index ];

			EditorGUILayout.BeginHorizontal();
			if ( GUILayout.Button("<<") )
			{
				Index = 0;
			}
			if ( GUILayout.Button("<") )
			{
				Index -= 1;
				if ( Index < 0 )
					Index = s.Rows.Count - 1;
			}
			if ( GUILayout.Button(">") )
			{
				Index += 1;
				if ( Index >= s.Rows.Count )
					Index = 0;
			}
			if ( GUILayout.Button(">>") )
			{
				Index = s.Rows.Count - 1;
			}

			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			GUILayout.Label( "ID", GUILayout.Width( 150.0f ) );
			{
				EditorGUILayout.LabelField( s.rowNames[ Index ] );
			}
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			GUILayout.Label( "_DESCRIPTION", GUILayout.Width( 150.0f ) );
			{
				EditorGUILayout.TextField( r._DESCRIPTION );
			}
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			GUILayout.Label( "_COLLECTION", GUILayout.Width( 150.0f ) );
			{
				EditorGUILayout.TextField( r._COLLECTION );
			}
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			GUILayout.Label( "_GAPS", GUILayout.Width( 150.0f ) );
			{
				EditorGUILayout.TextField( r._GAPS );
			}
			EditorGUILayout.EndHorizontal();

		}
	}
}
