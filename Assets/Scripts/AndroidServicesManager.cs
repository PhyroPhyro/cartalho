﻿using UnityEngine;
using System.Collections;

public class AndroidServicesManager : MonoBehaviour {

	void Start()
	{
		GooglePlayLogin ();
	}

	public void GooglePlayLogin()
	{
		GooglePlayConnection.ActionConnectionResultReceived += ActionConnectionResultReceived;
		GooglePlayConnection.Instance.connect ();
		GooglePlayManager.Instance.RetrieveDeviceGoogleAccounts();
	}
	
	private void ActionConnectionResultReceived(GooglePlayConnectionResult result) {
		if(result.IsSuccess) {
			Debug.Log("Connected!");
			//Application.LoadLevel("Rooms");
		} else {
			Debug.Log("Connection failed with code: " + result.code.ToString());
		}
	}
	
	public bool GooglePlayCheck()
	{
		if(GooglePlayConnection.state == GPConnectionState.STATE_CONNECTED) {
			return true;
		} 

		return false;
	}

	public GooglePlayerTemplate GooglePlayPlayer()
	{
		GooglePlayerTemplate currentPlayer = GooglePlayManager.Instance.player;
		return currentPlayer;
	}

	public void ShowAchievements()
	{
		GooglePlayManager.instance.showAchievementsUI ();
	}

	public void GoogleAchievement(string achievName)
	{
		switch (achievName) {
		case "Se corrompendo":
			GooglePlayManager.instance.UnlockAchievementById("CgkI9KT3ioEPEAIQBg");
			break;
		case "Bufao":
			GooglePlayManager.instance.UnlockAchievementById("CgkI9KT3ioEPEAIQAQ");
			break;
		}
	}	

	public void GoogleAchievIncrement(string achievName)
	{
		switch (achievName) {
		case "Full House":
			GooglePlayManager.instance.incrementAchievementById ("CgkI9KT3ioEPEAIQAA", 1);
			break;
		case "Blackjack":
			GooglePlayManager.instance.incrementAchievementById ("CgkI9KT3ioEPEAIQAg", 1);
			break;
		case "Parabens":
			GooglePlayManager.instance.incrementAchievementById ("CgkI9KT3ioEPEAIQAw", 1);
			break;
			
		}
	}
}
