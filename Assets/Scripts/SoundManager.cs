﻿using UnityEngine;
using System.Collections;

namespace Google2u
{
    public class SoundManager : MonoBehaviour
    {

        public AudioClip sndCard;
        public AudioClip sndClick;
        public AudioClip sndCzar;
        public AudioClip sndMainTheme;
        public AudioClip sndTheme1;
        public AudioClip sndTheme2;
        public AudioClip sndTheme3;
        public AudioClip sndRound;
        public AudioClip sndScore;
        public AudioClip sndEnd;

        public bool isSoundEnabled;

        public void playSound(AudioClip sound)
        {
            GetComponent<AudioSource>().PlayOneShot(sound);
        }

        public void DisableEnableSound()
        {
            if (isSoundEnabled)
            {
//                GameObject.Find("SoundManager").GetComponent<AudioSource>().mute = true;
                GameObject.Find("ThemeMusic").GetComponent<AudioSource>().mute = true;
                isSoundEnabled = false;

                Debug.Log("MUTANDO");
            }
            else
            {
//                GameObject.Find("SoundManager").GetComponent<AudioSource>().mute = false;
                GameObject.Find("ThemeMusic").GetComponent<AudioSource>().mute = false;
                isSoundEnabled = true;
            }
        }

        void Update()
        {
           /* if(Application.loadedLevelName == "Game")
            {
                if (GameObject.Find("ThemeMusic").GetComponent<AudioSource>().volume > 0)
                {
                    GameObject.Find("ThemeMusic").GetComponent<AudioSource>().volume -= 0.05f;
                }
            }
            else
            {
                if (GameObject.Find("ThemeMusic").GetComponent<AudioSource>().volume < 0.5f)
                {
                    GameObject.Find("ThemeMusic").GetComponent<AudioSource>().volume += 0.05f;
                }
            }*/
        }

        void OnLevelWasLoaded(int level)
        {
            if (Application.loadedLevelName == "Game")
            {
                int randomNumber = Random.Range(0, 4);

                switch(randomNumber)
                {
                    case 0:
                        GameObject.Find("ThemeMusic").GetComponent<AudioSource>().clip = sndTheme1;
                        break;
                    case 1:
                        GameObject.Find("ThemeMusic").GetComponent<AudioSource>().clip = sndTheme2;
                        break;
                    case 2:
                        GameObject.Find("ThemeMusic").GetComponent<AudioSource>().clip = sndTheme3;
                        break;
                    case 3:
                        GameObject.Find("ThemeMusic").GetComponent<AudioSource>().clip = sndTheme1;
                        break;
                    case 4:
                        GameObject.Find("ThemeMusic").GetComponent<AudioSource>().clip = sndTheme2;
                        break;
                }
            }
            else
            {
                GameObject.Find("ThemeMusic").GetComponent<AudioSource>().clip = sndMainTheme;
            }

            if (Application.loadedLevelName != "Splash")
            {
                if (!GameObject.Find("ThemeMusic").GetComponent<AudioSource>().isPlaying)
                {
                    GameObject.Find("ThemeMusic").GetComponent<AudioSource>().Play();
                }
            }

			if (Application.loadedLevelName == "Thanks" || Application.loadedLevelName == "Info") {
				GameObject.Find ("ThemeMusic").GetComponent<AudioSource> ().volume = 0;
			} else {
				GameObject.Find ("ThemeMusic").GetComponent<AudioSource> ().volume = 0.5f;
			}
        }
    }
}