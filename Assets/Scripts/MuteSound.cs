﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Google2u
{
    public class MuteSound : MonoBehaviour
    {

        public Sprite enableSound;
        public Sprite disableSound;

        void Start()
        {
            SoundManager sM = GameObject.Find("SoundManager").GetComponent<SoundManager>();

            if (!sM.isSoundEnabled)
            {
                GetComponent<Image>().sprite = disableSound;
            }
            else
            {
                GetComponent<Image>().sprite = enableSound;
            }
        }

        public void OnClick()
        {
            SoundManager sM = GameObject.Find("SoundManager").GetComponent<SoundManager>();

            if (sM.isSoundEnabled)
            {
                GetComponent<Image>().sprite = disableSound;
            }
            else
            {
                GetComponent<Image>().sprite = enableSound;
            }

            sM.DisableEnableSound();
        }
    }
}