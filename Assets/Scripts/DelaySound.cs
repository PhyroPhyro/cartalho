﻿using UnityEngine;
using System.Collections;

public class DelaySound : MonoBehaviour {

	void Start()
    {
        StartCoroutine(delayStart());
    }

    IEnumerator delayStart()
    {
        yield return new WaitForSeconds(0.6f);

        GetComponent<AudioSource>().Play();

        yield return new WaitForSeconds(4f);

        Application.LoadLevel("Main");
    }
}