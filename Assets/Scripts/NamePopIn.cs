﻿using UnityEngine;
using System.Collections;

public class NamePopIn : MonoBehaviour {

	public float scaleTo;
	public float delayInto = 0;

	void Start () {
		iTween.ScaleTo (gameObject, iTween.Hash ("x", scaleTo, "y", scaleTo, "z", scaleTo, "time", 1.5f, "delay", delayInto, "easetype", iTween.EaseType.easeOutElastic));
	}
}
