﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using StartApp;

namespace Google2u
{
    public class NetworkManager : MonoBehaviour
    {
        private const string typeName = "CardsAgainst";
       // private const string ipServerAdress = "45.55.54.59";
        public string gameName = "Puxadim";
        public string gamePass = "";
        private string gameCat = "";
        public string nickName = "Gibe Nick Plox!!1!";

        private bool isNick = false;
        private bool isServerName = false;
        private bool isServer = false;

		public bool isDebug = false;

        private bool isRefreshingHostList = false;
        private HostData[] hostList;

        public GameObject playerPrefab;

        public Texture2D bgInitialScreen;

		private int adCounter = 1;
		private float dropCounter = 40;
		private bool isFirstTime = true;

		void Start()
		{
			#if UNITY_ANDROID
			StartAppWrapper.init();
			StartAppWrapper.loadAd();
			#endif

			PhotonNetwork.ConnectUsingSettings("0.1");
		}
		 
		public void disconnectCancel()
		{
			PhotonNetwork.LeaveRoom ();
		}

		void Update()
		{
			//Debug.Log (PhotonNetwork.connectionState);

			if(PhotonNetwork.connected == false)
				PhotonNetwork.ConnectUsingSettings("0.1");

			if (PhotonNetwork.insideLobby == false)
				PhotonNetwork.JoinLobby ();

			if (GameObject.Find ("Online Counter"))
				GameObject.Find ("Online Counter").GetComponent<Text> ().text = PhotonNetwork.countOfPlayersOnMaster.ToString();
		}

        void Awake()
        {
            DontDestroyOnLoad(gameObject);
            Application.LoadLevel("Splash");
        }
        
        public void StartServer()
        {
            //Network.InitializeServer(5, 25000, !Network.HavePublicAddress());
			ExitGames.Client.Photon.Hashtable roomProps = new ExitGames.Client.Photon.Hashtable();
			roomProps.Add ("Password", gamePass);
			roomProps.Add ("Playing", 0);
			string[] roomPropsInLobby = { "Password", "Playing" };
			PhotonNetwork.CreateRoom(gameName,true,true,5,roomProps, roomPropsInLobby);
            
            //Network.incomingPassword = gamePass;

            //MasterServer.RegisterHost(typeName, gameName, "Aguardando");
        }

		void OnCreatedRoom ()
		{
           // SpawnPlayer();
           StartingLobby();
        }

        private void RefreshHostList()
        {
            if (!isRefreshingHostList)
            {
                isRefreshingHostList = true;
                MasterServer.RequestHostList(typeName);
            }
        }

		void OnFailedToConnectToPhoton(DisconnectCause cause)
		{
            Debug.Log("Could not connect to server: " + cause);

			GameObject.Find("ErrorPopup").GetComponent<OpenPopup>().openPopupElements(true);
		}

        void OnJoinedRoom()
        {
			Debug.Log("JOINED ROOM");
			// SpawnPlayer();
			if (PhotonNetwork.isMasterClient) {
				StartingLobby ();
				return;
			}

			GetComponent<LobbyManager> ().SendVerifyMyNick (PhotonNetwork.player.name, PhotonNetwork.player);
		}

        public void StartingLobby()
        {
            GetComponent<LobbyManager>().ServerRefreshList();
        }

		void OnPhotonPlayerConnected(PhotonPlayer player)
		{
			Debug.Log("Player connected from " + player.name);
			Handheld.Vibrate ();

			//Sound
			FindObjectOfType<SoundManager>().playSound(FindObjectOfType<SoundManager>().sndScore);

			if (PhotonNetwork.isMasterClient)
				GetComponent<LobbyManager>().ServerRefreshList();
		}
		
		/*void OnPlayerDisconnected(NetworkPlayer player)
        {
            Debug.Log("Clean up after player " + player);

            Network.RemoveRPCs(player);

            if (Application.loadedLevelName != "Rooms")
            {
                GetComponent<GameStatesManager>().RemoteEndGame(0, true);
				GetComponent<LobbyManager>().isSomeoneLeft = true;
            }

            GetComponent<LobbyManager>().ServerRefreshList();
            MasterServer.RegisterHost(typeName, gameName, "Aguardando");     

			
        }*/

		void OnPhotonPlayerDisconnected(PhotonPlayer player)
		{
			Debug.Log("Clean up after player " + player);


			PhotonNetwork.RemoveRPCs(player);
			
			if (Application.loadedLevelName != "Rooms")
			{
				GetComponent<GameStatesManager>().RemoteEndGame(0, true);
				GetComponent<LobbyManager>().isSomeoneLeft = true;
				Application.LoadLevel("Rooms");
				PhotonNetwork.LeaveRoom();
			}

			Handheld.Vibrate ();
			
			//Sound
			FindObjectOfType<SoundManager>().playSound(FindObjectOfType<SoundManager>().sndScore);
			
			GetComponent<LobbyManager>().ServerRefreshList();

			ExitGames.Client.Photon.Hashtable currRoomProps = PhotonNetwork.room.customProperties;
			string passValue = (string)currRoomProps ["Password"];

			ExitGames.Client.Photon.Hashtable newProps = new ExitGames.Client.Photon.Hashtable ();
			newProps.Add ("Password", passValue);
			newProps.Add ("Playing", 0);

			PhotonNetwork.room.SetCustomProperties (newProps);
		}

		void OnReceivedRoomListUpdate ()
		{
			Debug.Log (PhotonNetwork.GetRoomList ().Length);

			FindObjectOfType<RoomManager>().RefreshHostList ();
		}

		/* void OnDisconnectedFromServer(NetworkDisconnection info)
        {
            GetComponent<LobbyManager>().ClearLocalList();
            GetComponent<GameStatesManager>().RemoteEndGame(0, false);
            Application.LoadLevel("Rooms");

            if (Network.isServer)
                Debug.Log("Local server connection disconnected");
            else
                if (info == NetworkDisconnection.LostConnection)
                    Debug.Log("Lost connection to the server");
                else
                    Debug.Log("Successfully diconnected from the server");

			if (adCounter <= 0) {
				#if UNITY_ANDROID
				StartAppWrapper.showAd ();
				StartAppWrapper.loadAd ();
				#endif
				
				adCounter++;
				
			} else {
				adCounter = 0;
			}
			
        }*/

		void OnDisconnectedFromPhoton()
		{
			GetComponent<LobbyManager>().ClearLocalList();
			GetComponent<GameStatesManager>().RemoteEndGame(0, false);
			Application.LoadLevel("Rooms");

			/*if (adCounter <= 0) {
				#if UNITY_ANDROID
				StartAppWrapper.showAd ();
				StartAppWrapper.loadAd ();
				#endif
				
				adCounter++;
				
			} else {
				adCounter = 0;
			}*/
		}

		void OnApplicationFocus(bool focusStatus) {
			if (!focusStatus && !isDebug) {
				dropCounter -= Time.deltaTime;

				if (dropCounter <= 0)
					PhotonNetwork.LeaveRoom ();
			} else {
				dropCounter = 40;
			}
		}

		void OnLevelWasLoaded(int level) {
			if (Application.loadedLevelName == "Rooms" && isFirstTime)
			{
				#if UNITY_ANDROID
				StartAppWrapper.showAd ();
				StartAppWrapper.loadAd ();
				#endif

				isFirstTime = false;
			}
		}

       /* void SpawnPlayer()
        {
           // Application.LoadLevel("Game");      
            Network.Instantiate(playerPrefab, gameObject.transform.position, Quaternion.identity, 0);
        }*/
    }
}