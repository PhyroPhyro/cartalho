﻿using UnityEngine;
using System.Collections;

public class HideObject : MonoBehaviour {
    public GameObject hiddenObj;

    public void OnClick()
    {
        hiddenObj.SetActive(false);
    }
}
