﻿using UnityEngine;
using System.Collections;

namespace Google2u
{
    public class PlayCustomSound : MonoBehaviour
    {

        public AudioClip customSound;

        public void OnClick()
        {
            GameObject.Find("SoundManager").GetComponent<SoundManager>().playSound(customSound);
        }
    }
}