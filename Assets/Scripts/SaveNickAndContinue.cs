﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Google2u
{
    public class SaveNickAndContinue : MonoBehaviour
    {

        public string nextScene;

        void Start()
        {
            if(PlayerPrefs.HasKey("nickname"))
            {
                GameObject.FindObjectOfType<InputField>().text = PlayerPrefs.GetString("nickname");
            }
        }

        public void OnClick()
        {
			if (GameObject.FindObjectOfType<InputField> ().text.Length > 0) {
				GameObject.FindObjectOfType<NetworkManager> ().nickName = GameObject.FindObjectOfType<InputField> ().text;
				PlayerPrefs.SetString ("nickname", GameObject.FindObjectOfType<InputField> ().text);
				PhotonNetwork.playerName = GameObject.FindObjectOfType<InputField> ().text;
				Application.LoadLevel (nextScene);
			}
        }
    }
}