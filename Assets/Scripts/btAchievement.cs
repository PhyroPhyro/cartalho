﻿using UnityEngine;
using System.Collections;

public class btAchievement : MonoBehaviour {

	public void OnClick()
	{
		FindObjectOfType<AndroidServicesManager>().ShowAchievements();
	}

	void Update()
	{
		if (!FindObjectOfType<AndroidServicesManager> ().GooglePlayCheck ())
			gameObject.SetActive (false);
	}
}