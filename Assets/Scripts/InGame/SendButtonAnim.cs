﻿using UnityEngine;
using System.Collections;

public class SendButtonAnim : MonoBehaviour {

	void Start () {
		iTween.ScaleAdd(gameObject, iTween.Hash("amount",new Vector3(0.1f,0.1f,0.1f),"time",0.5f,"loopType", iTween.LoopType.pingPong, "easetype", iTween.EaseType.linear));
	}

}
