using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Google2u
{
	public class EmojisManager : MonoBehaviour {

		private PhotonView networkView;
		private float nCounter = 5;

		void Start()
		{
			networkView = FindObjectOfType<PhotonView>();
		}

		[PunRPC]
		public void ReceiveEmojis(string emojiString, string nickPlayer)
		{
			List<GameObject> playerList = FindObjectOfType<GameStatesManager> ().playersPrefabList;
			for(int i = 0; i < playerList.Count; i++)
			{
				if(playerList[i].transform.FindChild("Name").GetComponent<Text>().text == nickPlayer)
				{
					playerList[i].transform.FindChild(emojiString).GetComponent<EmojiAnimate>().OpenEmoji();
				}
			}
		}

		public void SendEmojis(string emojiString, string nickPlayer)
		{
			if (nCounter <= 0) {
				networkView.RPC ("ReceiveEmojis", PhotonTargets.All, emojiString, nickPlayer);
				nCounter = 5;
			}
		}

		void Update()
		{
			if (nCounter > 0) {
				nCounter -= Time.deltaTime;
			}
		}
	}
}