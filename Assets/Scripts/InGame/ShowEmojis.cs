﻿using UnityEngine;
using System.Collections;

public class ShowEmojis : MonoBehaviour {

	public void OnClick()
	{
		gameObject.transform.FindChild("GladEmoji").GetComponent<EmojiAnimate>().OpenEmojiChoice(0);
		gameObject.transform.FindChild("PissedEmoji").GetComponent<EmojiAnimate>().OpenEmojiChoice(0.2f);
		gameObject.transform.FindChild("FunnyEmoji").GetComponent<EmojiAnimate>().OpenEmojiChoice(0.4f);
	}

	public void CloseAllEmojis()
	{
		Debug.Log ("Closing all emojis...");
		gameObject.transform.FindChild("GladEmoji").GetComponent<EmojiAnimate>().CloseEmoji(0.4f);
		gameObject.transform.FindChild("PissedEmoji").GetComponent<EmojiAnimate>().CloseEmoji(0.2f);
		gameObject.transform.FindChild("FunnyEmoji").GetComponent<EmojiAnimate>().CloseEmoji(0);
	}
}
