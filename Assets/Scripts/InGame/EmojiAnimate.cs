﻿using UnityEngine;
using System.Collections;

public class EmojiAnimate : MonoBehaviour {

	public float scaleToOpen;
	public bool isOpenedChoice = false;

	public void OpenEmoji()
	{
		Debug.Log ("Opening one emoji...");
		iTween.ScaleTo (gameObject, iTween.Hash ("x", scaleToOpen, "y", scaleToOpen, "z", scaleToOpen, "time", 1.5f, "easetype", iTween.EaseType.easeOutElastic));
		iTween.ScaleTo (gameObject, iTween.Hash ("x", 0, "y", 0, "z", 0, "time", 1.5f, "delay",2.5f, "easetype", iTween.EaseType.easeInElastic));
	}

	public void CloseEmoji(float delayFloat)
	{
		isOpenedChoice = false;
		Debug.Log ("Closing one emoji...");
		iTween.ScaleTo (gameObject, iTween.Hash ("x", 0, "y", 0, "z", 0, "time", 1f, "delay", delayFloat, "easetype", iTween.EaseType.easeInElastic));
	}

	public void OpenEmojiChoice(float delayFloat)
	{
		if (!isOpenedChoice) {
			iTween.ScaleTo (gameObject, iTween.Hash ("x", scaleToOpen, "y", scaleToOpen, "z", scaleToOpen, "time", 1.5f, "delay", delayFloat, "easetype", iTween.EaseType.easeOutElastic));
			isOpenedChoice = true;
		} else {
			CloseEmoji(delayFloat);
		}
	}
}
