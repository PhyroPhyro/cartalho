﻿using UnityEngine;
using System.Collections;

namespace Google2u
{
public class btSendEmoji : MonoBehaviour {

	public void OnClick()
	{
			FindObjectOfType<EmojisManager> ().SendEmojis (gameObject.name, PhotonNetwork.playerName);
			FindObjectOfType<ShowEmojis> ().CloseAllEmojis ();
	}
}
}