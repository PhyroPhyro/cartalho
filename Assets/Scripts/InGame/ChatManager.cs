﻿using UnityEngine;
using System.Collections;

public class ChatManager : MonoBehaviour {

	public string stringToEdit = "Type your message here!";
	public string stringChat = "";

	public bool isUsingInput = false;

	public Vector2 scrollPosition;

	void OnGUI() {
		if (isUsingInput) {
			GUI.SetNextControlName ("BeginMessage");
			stringToEdit = GUI.TextField (new Rect (10, 10, 200, 20), stringToEdit, 25);
		}

		GUILayout.BeginArea(new Rect(10,30, 200, 200));

		scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Width(200), GUILayout.Height(200));
		GUILayout.Label(stringChat);
		GUILayout.EndScrollView();

		GUILayout.EndArea();
	}
}