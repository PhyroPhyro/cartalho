﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Google2u
{
    public class ShowBigCard : MonoBehaviour
    {
        public void OnClick()
        {
            string cardText = GetComponentInChildren<Text>().text;

            if (GameObject.Find("GeneralManager").GetComponent<GameStatesManager>().isCurrentCzar == false && GameObject.Find("GeneralManager").GetComponent<GameStatesManager>().isInteractive)
            {
                GameObject bigCard = GameObject.Find("GeneralManager").GetComponent<GameStatesManager>().bigHandCard;

                GameObject.Find("GeneralManager").GetComponent<GameStatesManager>().VisualSelectedCard(cardText);

                bigCard.SetActive(true);
                bigCard.GetComponentInChildren<Text>().text = cardText;
                bigCard.GetComponentInChildren<SendPrevCard>().cardObj = gameObject;

				GameObject.Find("GeneralManager").GetComponent<GameStatesManager>().startTextResize(bigCard.GetComponentInChildren<Text>());
            }
        }
    }
}
