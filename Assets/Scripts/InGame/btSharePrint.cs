﻿using UnityEngine;
using System.Collections;

public class btSharePrint : MonoBehaviour {

	public Texture2D shareTexture;

	public void OnClick()
	{
		AndroidSocialGate.StartShareIntent("Compartilhe o Cartalho!", "Venha jogar Cartalho comigo! https://play.google.com/store/apps/details?id=com.gametabula.cartalho");
	}
}
