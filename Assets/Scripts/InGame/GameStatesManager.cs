using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Google2u
{
    public class GameStatesManager : MonoBehaviour
    {

        #region Game States Info
        public enum GameStates
        {
            BeginGame,
            SortCzar,
            WaitingForPlayers,
            SendPlayedCards,
            WaitingForCzar,
            SendCheckWinner,
            WaitingForRestart,
            RestartRound,
            EndGame,
            PostEnd
        }

        public GameStates currentGameState = GameStates.BeginGame;
        #endregion

        #region Deck Categories Info
        public List<string> categoryList = new List<string>();
        #endregion

        #region Players Info
        public List<string> netPlayerList;
        public string Player1Nick;
        public string Player2Nick;
        public string Player3Nick;
        public string Player4Nick;
        public string Player5Nick;

        public bool Player1Ready;
        public bool Player2Ready;
        public bool Player3Ready;
        public bool Player4Ready;
        public bool Player5Ready;

        public bool isCurrentCzar = false;
        #endregion

        #region Basic Info
		private PhotonView photonView;
        private SoundManager sM;

        public string aguardandoString;
        public string aguardeCzarString;
        public string escolhaMaoString;
        public string escolhaCzarString;
        public string preparandoString;

        public List<Card> playerHand = new List<Card>();
        public List<Card> receivedCards = new List<Card>();
        public List<string> cardsStrings = new List<string>();
		public List<int> blackCardsArray = new List<int> ();
		public List<int> whiteCardsArray = new List<int> ();
        public Card currentCzarCard;
        public Card currentChoiceCard;
        public bool isInteractive;
        public bool isStarted = false;

        public PhotonPlayer currentCzarPlayer;
		public float nCounter = 30f;
        public int nCurrentCzar = 0;
		public bool isCounting = false;
		public bool isCounterTrigged = false;
        public string currentCzarNick;
        public string currentCzarChosenCard;

		public Sprite CzarTexture;
		public Sprite nonCzarTexture;
        #endregion

        #region Prefabs Info
        public GameObject handCardPrefab;
        public List<GameObject> handCardsList = new List<GameObject>();
        public GameObject czarChoicePrefab;
        public List<GameObject> chosenCardsList = new List<GameObject>();
        public GameObject playerPrefab;
        public List<GameObject> playersPrefabList = new List<GameObject>();
        public GameObject bigHandCard;
        #endregion

        #region Setup & States Machine
        void OnEnable()
        {
			photonView = gameObject.GetComponent<PhotonView>();

            sM = FindObjectOfType<SoundManager>();

            currentGameState = GameStates.BeginGame;

            ResetAllGame();

            if(PhotonNetwork.isMasterClient)
                StartCoroutine("BeginGameFunction");
        }

        void OnLevelWasLoaded(int level)
        {
            if (GameObject.Find("Big Hand Card"))
            {
                bigHandCard = GameObject.Find("Big Hand Card");
                bigHandCard.SetActive(false);
            }
        }

        void Update()
        {
           // Debug.Log(nCurrentCzar);

            if(Input.GetKeyDown(KeyCode.A))
            {
                GameObject.Find("Vencedor").GetComponent<OpenPopup>().openPopupElements(true);
                StartCoroutine(EndGame(6f));

                currentGameState = GameStates.EndGame;
            }

			if (Input.GetKeyDown (KeyCode.L)) {
				iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.3f, "y", 0.3f, "z", 0.3f, "time", 1));
				iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.2f, "y", 0.2f, "z", 0.2f, "time", 1, "delay", 0.5f));
			}
				
				if(GameObject.Find ("txCounter"))
				GameObject.Find ("txCounter").GetComponent<Text> ().text = nCounter.ToString ("n0");

			if (nCounter > 0 && isCounting) {
				nCounter -= Time.deltaTime;
				isCounterTrigged = false;
			} else {
				nCounter = 30;
				isCounterTrigged = true;
				isCounting = false;
			}

            switch (currentGameState)
            {
                case GameStates.BeginGame:
                    if (PhotonNetwork.isMasterClient)
                    {
                        Debug.Log("SOU SERVER");
                    }
                    else {
                        if (!isCurrentCzar)
                            isInteractive = true;
                        currentGameState = GameStates.WaitingForPlayers;
						RestartCounter();
                    }

                    if (isCurrentCzar)
                    {
                        GameObject.Find("txStatus").GetComponent<Text>().text = aguardandoString;
						iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.3f, "y", 0.3f, "z", 0.3f, "time", 1));
						iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.2f, "y", 0.2f, "z", 0.2f, "time", 1, "delay", 0.5f));
						VisualDisableCards(true);
                    }
                    else
                    {
                        GameObject.Find("txStatus").GetComponent<Text>().text = escolhaMaoString;
						iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.3f, "y", 0.3f, "z", 0.3f, "time", 1));
						iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.2f, "y", 0.2f, "z", 0.2f, "time", 1, "delay", 0.5f));
						VisualDisableCards(false);
                    }

                    break;
                case GameStates.SortCzar:
                    SortCzarFunction();
                    break;
                case GameStates.WaitingForPlayers:
                    WaitingForPlayersFunction();
                    break;
                case GameStates.SendPlayedCards:
                    SendPlayedCardsFunction();
                    break;
                case GameStates.WaitingForCzar:
					WaitingForCzarFunction();
                    break;
                case GameStates.WaitingForRestart:
					isCounting = false;
                    GameObject.Find("txStatus").GetComponent<Text>().text = preparandoString;
					iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.3f, "y", 0.3f, "z", 0.3f, "time", 1));
					iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.2f, "y", 0.2f, "z", 0.2f, "time", 1, "delay", 0.5f));
					VisualDisableCards(true);
                    break;
                case GameStates.SendCheckWinner:
					isCounting = false;
                    SendCheckWinnerFunction();
                    break;
                case GameStates.RestartRound:
                    RestartRoundFunction();
                    break;
            }
        }

		public void RestartCounter()
		{
			nCounter = 30;
			isCounting = true;
		}
        #endregion

        #region States Functions

        private void SortCzarFunction()
        {
            StartCoroutine(SortCzarCoroutine());
            currentGameState = GameStates.WaitingForPlayers;

			RestartCounter ();
        }

        private void WaitingForPlayersFunction()
        {
            if (PhotonNetwork.isMasterClient) {
				if(isCounterTrigged && receivedCards.Count > 1)
				{
					currentGameState = GameStates.SendPlayedCards;
				}else if(isCounterTrigged && receivedCards.Count <= 0)
				{
					RestartCounter();
				}else if(isCounterTrigged && receivedCards.Count == 1)
				{
					RPCReceiveCzarChoice(receivedCards[0].CardDescription);
				}
			}
        }

        private void SendPlayedCardsFunction()
        {
            Debug.Log("SENDING PLAYER CHOICE CARDS");
            isInteractive = false;

            for(int i = 0; i < receivedCards.Count; i++)
            {
                photonView.RPC("RPCReceiveChosenCards", PhotonTargets.All, receivedCards[i].CardDescription, receivedCards[i].PlayerChoice);
            }

            currentGameState = GameStates.WaitingForCzar;
			iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.3f, "y", 0.3f, "z", 0.3f, "time", 1));
			iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.2f, "y", 0.2f, "z", 0.2f, "time", 1, "delay", 0.5f));
			RestartCounter ();
        }

        private void WaitingForCzarFunction()
        {
            if (isCurrentCzar)
            {
                isInteractive = true;
                GameObject.Find("txStatus").GetComponent<Text>().text = escolhaCzarString;

				VisualDisableCards(false);
            }
            else
            {
                isInteractive = false;
                GameObject.Find("txStatus").GetComponent<Text>().text = aguardeCzarString;

				VisualDisableCards(true);
            }

			if (PhotonNetwork.isMasterClient) {
				if(isCounterTrigged)
				{
					RPCReceiveCzarChoice(receivedCards[Random.Range(0, receivedCards.Count-1)].CardDescription);
				}
			}
        }

        private void SendCheckWinnerFunction()
        {
            for(int i = 0; i < playersPrefabList.Count; i++)
            {
                if(playersPrefabList[i].transform.FindChild("Score").GetComponent<Text>().text == "5")
                {
                    GameObject.Find("Vencedor").GetComponent<OpenPopup>().openPopupElements(true);
                    GameObject.Find("WinnerName").GetComponent<Text>().text = playersPrefabList[i].transform.FindChild("Name").GetComponent<Text>().text;

					//ACHIEVEMENTS//
					if(FindObjectOfType<AndroidServicesManager>().GooglePlayCheck() && GetComponent<NetworkManager>().nickName == playersPrefabList[i].transform.FindChild("Name").GetComponent<Text>().text)
					{
						FindObjectOfType<AndroidServicesManager>().GoogleAchievement("Se corrompendo");
						FindObjectOfType<AndroidServicesManager>().GoogleAchievIncrement("Full House");
						FindObjectOfType<AndroidServicesManager>().GoogleAchievIncrement("Blackjack");
						FindObjectOfType<AndroidServicesManager>().GoogleAchievIncrement("Parabens");

						if(playersPrefabList.Count >= 5)
						{
							FindObjectOfType<AndroidServicesManager>().GoogleAchievement("Bufao");
						}
					}
					///////////////

                    StartCoroutine(EndGame(6f));

                    currentGameState = GameStates.EndGame;
                }
            }

            if(currentGameState != GameStates.EndGame)
            {
                currentGameState = GameStates.RestartRound;
            }
        }

        private void RestartRoundFunction()
        {          
            isCurrentCzar = false;
            Player1Ready = false;
            Player2Ready = false;
            Player3Ready = false;
            Player4Ready = false;
            Player5Ready = false;

            chosenCardsList.Clear();
            receivedCards.Clear();

            //Sound
            sM.playSound(sM.sndRound);
            //

            if (PhotonNetwork.isMasterClient)
            {
                Debug.Log("///////////////////////////////MANDAR NOVA CARTA E SORTEAR CZAR");
                StartCoroutine(NewHandCard(5, false));
                currentGameState = GameStates.SortCzar;
            }
            else
            {
                isInteractive = true;
                currentGameState = GameStates.WaitingForPlayers;
				RestartCounter();

                if (isCurrentCzar)
                {
                    GameObject.Find("txStatus").GetComponent<Text>().text = aguardandoString;
					iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.3f, "y", 0.3f, "z", 0.3f, "time", 1));
					iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.2f, "y", 0.2f, "z", 0.2f, "time", 1, "delay", 0.5f));
					VisualDisableCards(true);
                }
                else
                {
                    GameObject.Find("txStatus").GetComponent<Text>().text = escolhaMaoString;
					iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.3f, "y", 0.3f, "z", 0.3f, "time", 1));
					iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.2f, "y", 0.2f, "z", 0.2f, "time", 1, "delay", 0.5f));
					VisualDisableCards(false);
                }
            }
        }
        #endregion

        #region Setup Functions

        //Sort given amount of card, to every player (with restrictions)
        IEnumerator NewHandCard(int cardAmount, bool isFirstHand)
        {
            yield return new WaitForSeconds(0.5f);

            //Get white cards list
            WhiteCards whiteCardsClass = FindObjectOfType(typeof(WhiteCards)) as WhiteCards;
            List<WhiteCardsRow> whiteCardsSheet = whiteCardsClass.Rows;
			Debug.Log ("WHITE SHEETS COUNT " + whiteCardsSheet.Count);
			if (whiteCardsArray == null || whiteCardsArray.Count == 0) {
				for(int m = 0; m < whiteCardsSheet.Count; m ++)
				{
					whiteCardsArray.Add(m);
				}
			}
			Debug.Log (whiteCardsArray.Count + "//////////////////////ARAAAAAAAY///////////////////////////////");

			
			for (int i = 0; i <= (PhotonNetwork.playerList.Length); i++)
	            {
	                for (int k = 0; k < cardAmount; k++)
                	{

						int randomNumber = whiteCardsArray[Random.Range(0, whiteCardsArray.Count)];
						whiteCardsArray.Remove (randomNumber);

	                    Card newCard = new Card();
	                    newCard.CardID = randomNumber;
	                    newCard.CardDescription = whiteCardsSheet[randomNumber]._NAME;

	                    switch (i)
	                    {
	                        case 0:
	                            if (isFirstHand || currentCzarPlayer != PhotonNetwork.player)
	                                AddCardToHand(newCard.CardID, newCard.CardDescription);
	                            break;
	                        case 1:
							if (isFirstHand || currentCzarPlayer != PhotonNetwork.playerList[0])
		                                photonView.RPC("AddCardToHand", PhotonNetwork.playerList[0], newCard.CardID, newCard.CardDescription);
	                            break;
	                        case 2:
							if (isFirstHand || currentCzarPlayer != PhotonNetwork.playerList[1])
								photonView.RPC("AddCardToHand", PhotonNetwork.playerList[1], newCard.CardID, newCard.CardDescription);
	                            break;
	                        case 3:
							if (isFirstHand || currentCzarPlayer != PhotonNetwork.playerList[2])
								photonView.RPC("AddCardToHand", PhotonNetwork.playerList[2], newCard.CardID, newCard.CardDescription);
	                            break;
	                        case 4:
							if (isFirstHand || currentCzarPlayer != PhotonNetwork.playerList[3])
								photonView.RPC("AddCardToHand", PhotonNetwork.playerList[3], newCard.CardID, newCard.CardDescription);
	                            break;
	                        case 5:
							if (isFirstHand || currentCzarPlayer != PhotonNetwork.playerList[4])
								photonView.RPC("AddCardToHand", PhotonNetwork.playerList[4], newCard.CardID, newCard.CardDescription);
	                            break;
	                    }

	                    Debug.Log("MANDANDO CARTA " + newCard.CardID + " PARA PLAYER" + i.ToString());
                }
			}
        }

        //Creates a new card, if any card is with same text of another card in player's hand
        [PunRPC]
        public void RecreateNewCard(PhotonPlayer player)
        {
            //Get white cards list
            WhiteCards whiteCardsClass = FindObjectOfType(typeof(WhiteCards)) as WhiteCards;
            List<WhiteCardsRow> whiteCardsSheet = whiteCardsClass.Rows;

            int randomNumber = Random.Range(0, whiteCardsSheet.Count);

            Card newCard = new Card();
            newCard.CardID = randomNumber;
            newCard.CardDescription = whiteCardsSheet[randomNumber]._NAME;

            photonView.RPC("AddCardToHand", player, newCard.CardID, newCard.CardDescription);
        }

		//Resize cards text size
		public void startTextResize(Text cardTxt)
		{
			bool isResize = false;

			string[] arrayString = cardTxt.text.Split(" "[0]);
			for(int i = 0; i < arrayString.Length; i++)
			{
				if(arrayString[i].Length >= 9 || cardTxt.text.Length > 60)
				{
					cardTxt.fontSize = 24;
					isResize = true;
				}
				
				if(!isResize)
				{
					cardTxt.fontSize = 32;
				}
			}
		}

        //Starter function (server only)
        IEnumerator BeginGameFunction()
        {
            Debug.Log("NETWORK CONNECTIONS:" + PhotonNetwork.playerList.Length);

            yield return new WaitForSeconds(0.5f);

            StartCoroutine(NewHandCard(5, true));

            CreatePlayers();

            currentGameState = GameStates.SortCzar;
        }

        //Creating players, used by server, sending players do everyone
        public void CreatePlayers()
        {
            List<string> playerNamesList = GetComponent<LobbyManager>().listNames;
            for (int i = 0; i < playerNamesList.Count; i++)
            {
                photonView.RPC("SendCreatePlayer", PhotonTargets.All, playerNamesList[i], i);
            }
        }

        //Received by everyone, creates players visualy on GUI
        [PunRPC]
        public void SendCreatePlayer(string playerName, int playerNumber)
        {
            if (playerName != PhotonNetwork.playerName)
            {
                GameObject currPlayer = GameObject.Instantiate(playerPrefab);
                currPlayer.transform.SetParent(GameObject.Find("Players Grid").transform, false);
                //currPlayer.transform.parent = GameObject.Find("Players Grid").transform;

                currPlayer.transform.FindChild("Name").GetComponent<Text>().text = playerName;
                currPlayer.transform.FindChild("Score").GetComponent<Text>().text = "0";

                playersPrefabList.Add(currPlayer);
            }
            else
            {
                GameObject currPlayerObj = GameObject.Find("MyPlayer");

                currPlayerObj.transform.FindChild("Name").GetComponent<Text>().text = playerName;
                currPlayerObj.transform.FindChild("Score").GetComponent<Text>().text = "0";

                playersPrefabList.Add(currPlayerObj);
            }

            switch (playerNumber)
            {
                case 0:
                    Player1Nick = playerName;
                    break;
                case 1:
                    Player2Nick = playerName;
                    break;
                case 2:
                    Player3Nick = playerName;
                    break;
                case 3:
                    Player4Nick = playerName;
                    break;
                case 4:
                    Player5Nick = playerName;
                    break;
            }
        }

        //Received by everyone, used to receive new cards and visualy adds it to screen
        [PunRPC]
        public void AddCardToHand(int cardID, string cardText)
        {
			if(playerHand.Count < 5){
	            Card currCard = new Card();
	            currCard.CardID = cardID;
	            currCard.CardDescription = cardText;
	            playerHand.Add(currCard);

	            Debug.Log("TEXTO CARTA: " + currCard.CardDescription);
	            Debug.Log("ID CARTA: " + currCard.CardID);

	            RefreshHand();
			}
        }

        //Used by everyone to visualy refresh the cards in hand
        public void RefreshHand()
        {
            Debug.Log("ANTES DE DESTRUIR MÃO, TINHA " + handCardsList.Count + " CARTAS");
            for (int i = 0; i < handCardsList.Count; i++)
            {
                Destroy(handCardsList[i]);
            }
            handCardsList = new List<GameObject>();

            Debug.Log("DEPOIS DE DESTRUIR MÃO, TEM " + handCardsList.Count + " CARTAS");

            cardsStrings.Clear();

            for (int k = 0; k < playerHand.Count; k++)
            {
                GameObject currHandCard = GameObject.Instantiate(handCardPrefab);
                currHandCard.transform.SetParent(GameObject.Find("Hand Grid").transform, false);

                if(cardsStrings.Contains(playerHand[k].CardDescription))
                {
                    //Get white cards list
                    WhiteCards whiteCardsClass = FindObjectOfType(typeof(WhiteCards)) as WhiteCards;
                    List<WhiteCardsRow> whiteCardsSheet = whiteCardsClass.Rows;

                    int randomNumber = Random.Range(0, whiteCardsSheet.Count);

                    playerHand[k].CardDescription = whiteCardsSheet[randomNumber]._NAME;
                }

                currHandCard.GetComponentInChildren<Text>().text = playerHand[k].CardDescription;
				startTextResize(currHandCard.GetComponentInChildren<Text>());
                cardsStrings.Add(playerHand[k].CardDescription);
                handCardsList.Add(currHandCard);
            }

            Debug.Log("PLAYERHAND: " + playerHand.Count);
        }

        #endregion

        #region Czar Functions

        IEnumerator SortCzarCoroutine()
        {
            //Sort and define new Czar

            yield return new WaitForSeconds(0.5f);

            if (nCurrentCzar >= (PhotonNetwork.playerList.Length))
            {
                nCurrentCzar = 0;
            }


            currentCzarPlayer = PhotonNetwork.playerList[nCurrentCzar];
			Debug.Log ("CZAR PLAYER " + currentCzarPlayer.name);
            photonView.RPC("SetCurrentCzar", currentCzarPlayer, true);

            nCurrentCzar++;

            //Sort and define new Czar Card
            //Get black cards list
            BlackCards blackCardsClass = FindObjectOfType(typeof(BlackCards)) as BlackCards;
            List<BlackCardsRow> blackCardsSheet = blackCardsClass.Rows;

			Debug.Log ("BLACK COUNT " + blackCardsArray.Count);

			if (blackCardsArray == null || blackCardsArray.Count == 0) {
				for(int i = 0; i < blackCardsSheet.Count; i ++)
				{
					blackCardsArray.Add(i);
				}
			}
			Debug.Log (blackCardsArray.Count + "///////////////////////////ARAAAAAAAY//////////////////////////");

			int randomNumber = blackCardsArray[Random.Range(0, blackCardsArray.Count)];
			blackCardsArray.Remove (randomNumber);

            Card newBlackCard = new Card();
            newBlackCard.CardID = randomNumber;
            newBlackCard.CardDescription = blackCardsSheet[randomNumber]._DESCRIPTION;

            photonView.RPC("SetCzarCard", PhotonTargets.All, newBlackCard.CardID, newBlackCard.CardDescription, int.Parse(blackCardsSheet[randomNumber]._GAPS));

            Debug.Log("CZAR CARD: " + newBlackCard.CardDescription);

            if (!isCurrentCzar)
                isInteractive = true;

            if (isCurrentCzar)
            {
                GameObject.Find("txStatus").GetComponent<Text>().text = aguardandoString;
				iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.3f, "y", 0.3f, "z", 0.3f, "time", 1));
				iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.2f, "y", 0.2f, "z", 0.2f, "time", 1, "delay", 0.5f));
				VisualDisableCards(true);
            }
            else
            {
                GameObject.Find("txStatus").GetComponent<Text>().text = escolhaMaoString;
				iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.3f, "y", 0.3f, "z", 0.3f, "time", 1));
				iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.2f, "y", 0.2f, "z", 0.2f, "time", 1, "delay", 0.5f));
				VisualDisableCards(false);
            }
        }

        //Received by the Czar player (only)
        [PunRPC]
        public void SetCurrentCzar(bool isCzar)
        {
            Debug.Log("CURRENT CZAR " + isCzar);
            isCurrentCzar = isCzar;

            if (isCzar)
            {
                GameObject.Find("txStatus").GetComponent<Text>().text = aguardandoString;
				VisualDisableCards(true);

                //Sound
                sM.playSound(sM.sndCzar);
                //

                if (PhotonNetwork.isMasterClient == false)
                    photonView.RPC("SetCzarNick", PhotonTargets.MasterClient, PhotonNetwork.playerName);
                else
					SetCzarNick(PhotonNetwork.playerName);
            }
        }

        //Received by Server (only) and set Czar name
        [PunRPC]
        public void SetCzarNick(string CzarName)
        {
            Debug.Log("CZAR NICK HERE: " + CzarName);
            currentCzarNick = CzarName;

            photonView.RPC("SetVisualCzar", PhotonTargets.All, CzarName);
        }

        //Received by everyone, set visual Czar
        [PunRPC]
        public void SetVisualCzar(string CzarName)
        {
            for (int i = 0; i < playersPrefabList.Count; i++)
            {
                if (playersPrefabList[i].transform.FindChild("Name").GetComponent<Text>().text == CzarName)
                {
					playersPrefabList[i].GetComponent<Image>().sprite = CzarTexture;
					GameObject.Find("ReiPopup").GetComponent<SetReiAnnouncer>().SetAnnouncer(CzarName);
                }
                else
                {
					playersPrefabList[i].GetComponent<Image>().sprite = nonCzarTexture;
                }
            }
        }

        //Received by everyone with black card text
        [PunRPC]
        public void SetCzarCard(int cardID, string cardText, int cardGaps)
        {
            Card currCzarCard = new Card();
            currCzarCard.CardID = cardID;
            currCzarCard.CardDescription = cardText;

            Debug.Log("TEXTO CARTA: " + currCzarCard.CardDescription);
            Debug.Log("ID CARTA: " + currCzarCard.CardID);

            GameObject.Find("Czar Card").GetComponentInChildren<Text>().text = currCzarCard.CardDescription;
			startTextResize (GameObject.Find ("Czar Card").GetComponentInChildren<Text> ());
            currentCzarCard = currCzarCard;

            //FECHAR LOADING
            if(isStarted == false)
            {
				if(GameObject.Find("Carregando"))
	                GameObject.Find("Carregando").SetActive(false);

                isStarted = true;
            }
        }
        
        //Received by Czar with Chosen Card text, by Czar
        public void ReceiveCzarChoice(string cardText)
        {
            if (PhotonNetwork.isMasterClient)
            {
                RPCReceiveCzarChoice(cardText);
            }
            else
            {
                photonView.RPC("RPCReceiveCzarChoice", PhotonTargets.MasterClient, cardText);
            }
        }
		
        //Received by Server (only) with chosen card text
        [PunRPC]
        public void RPCReceiveCzarChoice(string cardText)
        {
            string playerChosen = "";

            Debug.Log("RECEBENDO CZAR");

            for (int i = 0; i < receivedCards.Count; i++)
            {
                if (cardText == receivedCards[i].CardDescription)
                    playerChosen = receivedCards[i].PlayerChoice;
            }

            photonView.RPC("IncreasePlayerPoints", PhotonTargets.All, playerChosen, cardText);
        }

        #endregion

        #region Basic Functions

        //Send (by everyone) to server the chosen card to fill the black card
        public void SendChoiceCard(string cardText, string nickName)
        {
            bool isReady = false;

            Debug.Log("MAXIMO DE CARTAS ENVIADAS");
            VisualDisableCards(true);
            isInteractive = false;
            isReady = true;

            GameObject.Find("txStatus").GetComponent<Text>().text = aguardandoString;
			iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.3f, "y", 0.3f, "z", 0.3f, "time", 1));
			iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.2f, "y", 0.2f, "z", 0.2f, "time", 1, "delay", 0.5f));

            if(PhotonNetwork.isMasterClient)
            {
                RPCSendChoiceCard(cardText, nickName, isReady);
            }
            else
            {
				photonView.RPC("RPCSendChoiceCard", PhotonTargets.MasterClient, cardText, nickName, isReady);
            }
        }

        //Used by server (only) to manage received chosen cards and which players are ready
        [PunRPC]
        public void RPCSendChoiceCard(string cardText, string playerNickName, bool isReady)
        {
            Debug.Log("CARD RECEIVED: " + cardText);
            Debug.Log("CARD SENDER: " + playerNickName);
            Debug.Log("SENDER READY: " + isReady);

            Card cardObj = new Card();
            cardObj.CardDescription = cardText;
            cardObj.PlayerChoice = playerNickName;

            receivedCards.Add(cardObj);

            if (cardObj.PlayerChoice == Player1Nick)
                Player1Ready = true;
            else if (cardObj.PlayerChoice == Player2Nick)
                Player2Ready = true;
            else if (cardObj.PlayerChoice == Player3Nick)
                Player3Ready = true;
            else if (cardObj.PlayerChoice == Player4Nick)
                Player4Ready = true;
            else if (cardObj.PlayerChoice == Player5Nick)
                Player5Ready = true;

            if (isReady)
            {
                CheckPlayerCards(playerNickName, isReady);
            }
        }

        //Used by server (only) to check if all players are ready to Czar choice
        public void CheckPlayerCards(string playerNick, bool isReady)
        {
			bool isDone = true;

			if (currentCzarNick == Player1Nick) {
				Player1Ready = true;
			} else if (currentCzarNick == Player2Nick) {
				Player2Ready = true;
			} else if (currentCzarNick == Player3Nick) {
				Player3Ready = true;
			} else if (currentCzarNick == Player4Nick) {
				Player4Ready = true;
			} else if (currentCzarNick == Player5Nick) {
				Player5Ready = true;
			}

            Debug.Log("CZAR NAME: " + currentCzarNick);

            List<string> playerNamesList = GetComponent<LobbyManager>().listNames;
            for (int i = 0; i < playerNamesList.Count; i++)
            {
                switch(i)
                {
                    case 0:
                        if (!Player1Ready && playerNick != currentCzarNick)
                        {
                            if (playerNick != currentCzarNick)
                            {
                                isDone = false;
                                Debug.Log("PLAYER 1 NAO TA PRONTO");
                            }
                        }
                        break;
                    case 1:
                        if (!Player2Ready && playerNick != currentCzarNick)
                        {
                            if (playerNick != currentCzarNick)
                            {
                                isDone = false;
                                Debug.Log("PLAYER 2 NAO TA PRONTO");
                            }
                        }
                        break;
                    case 2:
                        if (!Player3Ready && playerNick != currentCzarNick)
                        {
                            if (playerNick != currentCzarNick)
                            {
                                isDone = false;
                                Debug.Log("PLAYER 3 NAO TA PRONTO");
                            }
                        }
                        break;
                    case 3:
                        if (!Player4Ready && playerNick != currentCzarNick)
                        {
                            if (playerNick != currentCzarNick)
                            {
                                isDone = false;
                                Debug.Log("PLAYER 4 NAO TA PRONTO");
                            }
                        }
                        break;
                    case 4:
                        if (!Player5Ready && playerNick != currentCzarNick)
                        {
                            if (playerNick != currentCzarNick)
                            {
                                isDone = false;
                                Debug.Log("PLAYER 5 NAO TA PRONTO");
                            }
                        }
                        break;
                }
            }

            if (isDone)
                currentGameState = GameStates.SendPlayedCards;

            Debug.Log("IS DONE " + isDone);
            Debug.Log("Current Game State " + currentGameState.ToString());
        }

        //Turn all cards into gray
        public void VisualDisableCards(bool isGray)
        {
            SelectSendCard[] cardObjs = GameObject.FindObjectsOfType<SelectSendCard>();

            
            for(int i = 0; i < cardObjs.Length; i++)
            {
                if (isGray)
                {
                    cardObjs[i].GetComponent<Button>().interactable = false;
					cardObjs[i].GetComponent<Image>().color = new Color(145, 145, 145, 255);
                }
                else
                {
                    if (!isCurrentCzar)
                    {
                        cardObjs[i].GetComponent<Button>().interactable = true;
                        cardObjs[i].GetComponent<Image>().color = new Color(255, 255, 255, 255);
                        handCardsList[i].GetComponent<Button>().interactable = true;
                        handCardsList[i].GetComponent<Image>().color = new Color(255, 255, 255, 255);
                    }
                    else
                    {
                        handCardsList[i].GetComponent<Button>().interactable = false;
                        handCardsList[i].GetComponent<Image>().color = new Color(145, 145, 145, 255);
                    }
                }
            }
        }

        //Received by everyone to show which cards was chosen by players
        [PunRPC]
        public void RPCReceiveChosenCards(string cardText, string senderNick)
        {
            GameObject currCard = GameObject.Instantiate(czarChoicePrefab);
            currCard.GetComponentInChildren<Text>().text = cardText;
			startTextResize (currCard.GetComponentInChildren<Text> ());
            currCard.GetComponent<SelectSendCard>().isCzarChoice = true;

            if (GameObject.Find("Chosen Container 1").transform.childCount == 0)
           {
               currCard.transform.SetParent(GameObject.Find("Chosen Container 1").transform,false);
           }
           else if (GameObject.Find("Chosen Container 2").transform.childCount == 0)
           {
               currCard.transform.SetParent(GameObject.Find("Chosen Container 2").transform, false);
           }
            else if (GameObject.Find("Chosen Container 3").transform.childCount == 0)
           {
               currCard.transform.SetParent(GameObject.Find("Chosen Container 3").transform, false);
           }
            else if (GameObject.Find("Chosen Container 4").transform.childCount == 0)
           {
               currCard.transform.SetParent(GameObject.Find("Chosen Container 4").transform, false);
           }
            else if (GameObject.Find("Chosen Container 5").transform.childCount == 0)
           {
               currCard.transform.SetParent(GameObject.Find("Chosen Container 5").transform, false);
           }

            chosenCardsList.Add(currCard);

            currentGameState = GameStates.WaitingForCzar;
			iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.3f, "y", 0.3f, "z", 0.3f, "time", 1));
			iTween.ScaleTo (GameObject.Find ("txStatus"), iTween.Hash ("x", 0.2f, "y", 0.2f, "z", 0.2f, "time", 1, "delay", 0.5f));
			RestartCounter ();
        }

        //Used by everyone to receive, from server, which player scored a point
        [PunRPC]
        public void IncreasePlayerPoints(string playerName, string cardText)
        {
            for(int i = 0; i < playersPrefabList.Count; i++)
            {
                string currPlayerName = playersPrefabList[i].transform.FindChild("Name").GetComponent<Text>().text;
                int currPlayerScore = int.Parse(playersPrefabList[i].transform.FindChild("Score").GetComponent<Text>().text);

                if(playerName == currPlayerName)
                {
                    currPlayerScore++;

                    string newString = currPlayerName + " " + currPlayerScore.ToString();
                    playersPrefabList[i].transform.FindChild("Score").GetComponent<Text>().text = currPlayerScore.ToString();

					iTween.ScaleAdd(playersPrefabList[i], iTween.Hash("x",0.5f,"y", 0.5f,"z",0.5f,"time",1));
					iTween.ShakeRotation(playersPrefabList[i], iTween.Hash("z",5f,"time",3));
					iTween.ScaleAdd(playersPrefabList[i], iTween.Hash("x",-0.5f,"y",-0.5f,"z",-0.5f,"time",1,"delay",4));
                    Debug.Log("CURRENT CHOSEN PLAYER: " + currPlayerName + " AND POINTS: " + currPlayerScore);
                }
            }

            if(playerName == PhotonNetwork.playerName)
            {
                //Sound
                sM.playSound(sM.sndScore);
                //
            }

            currentGameState = GameStates.WaitingForRestart;

            StartCoroutine(ShowChosenCard(cardText));
        }

        //Used by everyone to show which card was chosen by Czar
        IEnumerator ShowChosenCard(string cardText)
        {
            for (int i = 0; i < chosenCardsList.Count; i++)
            {
                Destroy(chosenCardsList[i]);
            }
			
            isInteractive = false;

            GameObject currCard = GameObject.Instantiate(czarChoicePrefab);
            currCard.GetComponentInChildren<Text>().text = cardText;
			startTextResize (currCard.GetComponentInChildren<Text> ());
            currCard.transform.SetParent(GameObject.Find("Chosen Container 1").transform, false);

            yield return new WaitForSeconds(4f);

            Destroy(currCard);

            currentGameState = GameStates.SendCheckWinner;
        }

        //Used by everyone to deled a card from hand and then refresh all of them
        [PunRPC]
        public void RemoveCardFromHand(string currText)
        {
            for (int i = 0; i < playerHand.Count; i++)
            {
                if (playerHand[i].CardDescription == currText)
                {
                    playerHand.RemoveAt(i);
                }
            }

            Debug.Log("REMOVED CARD: " + currText);

            RefreshHand();
        }

        public void RemoteEndGame(float seconds, bool isSomeoneLeft)
        {
			photonView.RPC("ReceivingEndGame", PhotonTargets.All, seconds, isSomeoneLeft);
        }

		[PunRPC]
		public void ReceivingEndGame(float seconds, bool isSomeoneLeft)
		{
			Debug.Log("Saindo do Game");

			GetComponent<LobbyManager>().isSomeoneLeft = isSomeoneLeft;

			Debug.Log("Alguem saiu? " + isSomeoneLeft);

            StartCoroutine(EndGame(seconds,isSomeoneLeft));
		}

        public void ResetAllGame()
        {
            categoryList.Clear();
            playerHand.Clear();
            receivedCards.Clear();
            playersPrefabList.Clear();
            chosenCardsList.Clear();

            isCurrentCzar = false;
            Player1Ready = false;
            Player2Ready = false;
            Player3Ready = false;
            Player4Ready = false;
            Player5Ready = false;

            if (PhotonNetwork.isMasterClient) {
				ExitGames.Client.Photon.Hashtable currRoomProps = PhotonNetwork.room.customProperties;
				string passValue = (string)currRoomProps ["Password"];
				
				ExitGames.Client.Photon.Hashtable newProps = new ExitGames.Client.Photon.Hashtable ();
				newProps.Add ("Password", passValue);
				newProps.Add ("Playing", 0);
				
				PhotonNetwork.room.SetCustomProperties (newProps);
			}

            nCurrentCzar = 0;
        }

        public void VisualSelectedCard(string cardText)
        {
            for(int i = 0; i < handCardsList.Count; i++)
            {
                if (handCardsList[i].GetComponentInChildren<Text>().text == cardText)
                    handCardsList[i].GetComponent<Image>().color = new Color(255, 255, 0, 255);
                else
                    handCardsList[i].GetComponent<Image>().color = new Color(255, 255, 255, 255);
            }
        }

        //Used by everyone to end the game
        IEnumerator EndGame(float seconds, bool isError = false)
        {
            isStarted = false;

			if (!isError) {
				if (GameObject.Find ("WinnerName")) {
					iTween.ScaleTo (GameObject.Find ("WinnerName"), iTween.Hash ("x", 0.7f, "y", 0.7f, "z", 0.7f, "time", 1.5f, "easetype", iTween.EaseType.easeOutElastic));
					iTween.ScaleTo (GameObject.Find ("WinnerText"), iTween.Hash ("x", 0.7f, "y", 0.7f, "z", 0.7f, "time", 1.5f, "delay", 0.25f, "easetype", iTween.EaseType.easeOutElastic));
				}

				yield return new WaitForSeconds (0.75f);

				//Sound
				sM.playSound (sM.sndEnd);
				//

				yield return new WaitForSeconds (2.7f);
			}
            if(PhotonNetwork.isMasterClient)
                GetComponent<NetworkLevelLoad>().SetLoadLevel("Rooms");

            ResetAllGame();
        }

        #endregion
    }
}