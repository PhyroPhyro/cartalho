﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Google2u
{
    public class SelectSendCard : MonoBehaviour
    {

        public bool isCzarChoice;
        public Text currentText;

        public void SendCard()
        {
            if (currentText == null)
            {
                string cardText = GetComponentInChildren<Text>().text;
                currentText.text = cardText;
            }

            if (GameObject.Find("GeneralManager").GetComponent<GameStatesManager>().isInteractive)
            {
                Debug.Log("CLICANDO CARD");

                if (!isCzarChoice)
                {
                    if (!GameObject.Find("GeneralManager").GetComponent<GameStatesManager>().isCurrentCzar)
                    {
                        GameObject.Find("GeneralManager").GetComponent<GameStatesManager>().SendChoiceCard(currentText.text, GameObject.Find("GeneralManager").GetComponent<NetworkManager>().nickName);
                        GameObject.Find("GeneralManager").GetComponent<GameStatesManager>().RemoveCardFromHand(currentText.text);
                        Destroy(gameObject);
                    }
                }
                else
                {
                    if(GameObject.Find("GeneralManager").GetComponent<GameStatesManager>().isCurrentCzar)
                        GameObject.Find("GeneralManager").GetComponent<GameStatesManager>().ReceiveCzarChoice(currentText.text);
                }
            }
        }
    }
}