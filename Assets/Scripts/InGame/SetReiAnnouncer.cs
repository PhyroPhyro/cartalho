﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SetReiAnnouncer : MonoBehaviour {

	public GameObject Img1;
	public GameObject Img2;
	public GameObject Img3;

	public GameObject NameTxt;
	public GameObject TxtBar;

	public void SetAnnouncer(string czarName)
	{
		NameTxt.GetComponent<Text> ().text = czarName;

		iTween.MoveTo(Img1, iTween.Hash("x", GameObject.Find("ReiImg1Pos").transform.parent.position.x,"time",1));
		iTween.MoveTo(Img2, iTween.Hash("x", GameObject.Find("ReiImg2Pos").transform.parent.position.x,"time",1,"delay", 0.1f));
		iTween.MoveTo(Img3, iTween.Hash("x", GameObject.Find("ReiImg3Pos").transform.parent.position.x,"time",1,"delay", 0.2f));
		iTween.MoveTo(NameTxt, iTween.Hash("x", GameObject.Find("ReiNamePos").transform.parent.position.x,"time",1,"delay",0.15f));
		iTween.MoveTo(TxtBar, iTween.Hash("x", GameObject.Find("ReiTextPos").transform.parent.position.x,"time",1,"delay",0.2f));

		iTween.MoveTo(Img1, iTween.Hash("x",-1940f,"time",1,"delay",1.5f));
		iTween.MoveTo(Img2, iTween.Hash("x",-1940f,"time",1,"delay", 1.6f));
		iTween.MoveTo(Img3, iTween.Hash("x",-1940f,"time",1,"delay", 1.7f));
		iTween.MoveTo(NameTxt, iTween.Hash("x",-1940f,"time",1,"delay",1.6f));
		iTween.MoveTo(TxtBar, iTween.Hash("x",-1940f,"time",1,"delay",1.65f));

		iTween.MoveTo(Img1, iTween.Hash("x",3200f,"time",0,"delay",3f));
		iTween.MoveTo(Img2, iTween.Hash("x",3200f,"time",0,"delay", 3f));
		iTween.MoveTo(Img3, iTween.Hash("x",3200f,"time",0,"delay", 3f));
		iTween.MoveTo(NameTxt, iTween.Hash("x",3200f,"time",0,"delay",3f));
		iTween.MoveTo(TxtBar, iTween.Hash("x",3200f,"time",0,"delay",3f));
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.B)){
			SetAnnouncer("123123");
		}
	}
}
