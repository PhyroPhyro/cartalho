﻿using UnityEngine;
using System.Collections;

namespace Google2u
{
    public class SendPrevCard : MonoBehaviour
    {
        public GameObject cardObj;

        public void OnClick()
        {
            Debug.Log("IS INTERACTIVE: " + GameObject.Find("GeneralManager").GetComponent<GameStatesManager>().isInteractive);
            if(GameObject.Find("GeneralManager").GetComponent<GameStatesManager>().isInteractive)
                cardObj.GetComponent<SelectSendCard>().SendCard();
        }
    }
}