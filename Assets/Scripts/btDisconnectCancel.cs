﻿using UnityEngine;
using System.Collections;

namespace Google2u
{
public class btDisconnectCancel : MonoBehaviour {

	public void OnClick()
	{
		FindObjectOfType<NetworkManager> ().disconnectCancel ();
		GameObject.Find("Carregando").GetComponent<OpenPopup>().openPopupElements(false);
	}
}
}