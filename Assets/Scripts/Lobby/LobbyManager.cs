using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


namespace Google2u
{
    public class LobbyManager : MonoBehaviour
    {

        public bool isReady;
        public List<string> listNames = new List<string>();
        public List<GameObject> prefabList = new List<GameObject>();
        public GameObject playerPrefab;
        public GameObject carregandoWarning;
        public GameStatesManager gameStatesClass;

        public GameObject btPlayGame;
        public Text txPlayerNames;
        public string currHost;

        public bool isLoaded = false;
        public bool isSomeoneLeft = false;
		public bool isFromInGame = false;

        void OnLevelWasLoaded(int level)
        {
            if(Application.loadedLevelName == "Rooms")
            {


				if(PhotonNetwork.room != null)
                {
                    GetComponent<GameStatesManager>().ResetAllGame();

                    StartCoroutine(StartNewRefresh());
                }

				if (isSomeoneLeft)
				{
					GameObject.Find("SomeoneLeftPopup").GetComponent<OpenPopup>().openPopupElements(true);
					isSomeoneLeft = false;
				}
            }
        }


        IEnumerator StartNewRefresh()
        {
            yield return new WaitForSeconds(0.2f);

            if (PhotonNetwork.isMasterClient)
                ServerRefreshList();

          //  ClearLocalList();
        }

        //ALL
        [PunRPC]
        public void ClearLocalList()
        {
            listNames.Clear();
            for (int i = 0; i < prefabList.Count; i++)
            {
                Destroy(prefabList[i]);
            }
            prefabList.Clear();
            Debug.Log("CLEAR LIST");
        }

        //OTHERS
        [PunRPC]
        public void ReceivePlayerToList(string playerName, bool isServer)
        {
            listNames.Add(playerName);

            if (isServer)
                currHost = playerName;

            Debug.Log("RECEIVING NICK: " + playerName);
        }

        //ALL
        [PunRPC]
        public void RefreshLocalText()
        {
            for (int i = 0; i < prefabList.Count; i++)
            {
                Destroy(prefabList[i]);
            }

            if (GameObject.Find("txCarregandoPlayers") != null)
              carregandoWarning = GameObject.Find("txCarregandoPlayers");

            Debug.Log("REFRESH LOCAL TEXT");

			GameObject.Find ("RoomName").GetComponent<Text> ().text = PhotonNetwork.room.name;

            for (int i = 0; i < listNames.Count; i++)
            {
                GameObject currPlayerPrefab;
                if (listNames[i] == currHost || (listNames[i] == PhotonNetwork.player.name && PhotonNetwork.isMasterClient))
                {
                    currPlayerPrefab = GameObject.Find("HostSlice");
                    currPlayerPrefab.transform.FindChild("Name").GetComponent<Text>().text = "<b>" + listNames[i] + "</b>";
                }
                else
                {
                    currPlayerPrefab = GameObject.Instantiate(playerPrefab);
                    currPlayerPrefab.transform.FindChild("Name").GetComponent<Text>().text = listNames[i];
                    prefabList.Add(currPlayerPrefab);
                    currPlayerPrefab.transform.SetParent(GameObject.Find("PlayerList").transform, false);
                }
            }

            carregandoWarning.SetActive(false);
        }

        //CLIENT ONLY
        [PunRPC]
        public void SendPlayerNick()
        {
			FindObjectOfType<PhotonView>().RPC("ReceivePlayerToList", PhotonTargets.MasterClient, PhotonNetwork.player.name, false);

            if (carregandoWarning != null)
                carregandoWarning.SetActive(true);
        }

        //SERVER ONLY
        public void ServerRefreshList()
        {
            if (PhotonNetwork.isMasterClient)
            {
                Debug.Log("REFRESHANDO");

				FindObjectOfType<PhotonView>().RPC("SetGameManagerComp", PhotonTargets.All, false);

				FindObjectOfType<PhotonView>().RPC("ClearLocalList", PhotonTargets.All);

				listNames.Add(PhotonNetwork.player.name);

				FindObjectOfType<PhotonView>().RPC("SendPlayerNick", PhotonTargets.Others);

                StartCoroutine(ServerSendPlayerNicks());
            }
        }

        //CLIENT ONLY
        public void SendVerifyMyNick(string nickName, PhotonPlayer newcomer)
        {
            Debug.Log("MANDANDO PROPRIO NICK");
			FindObjectOfType<PhotonView>().RPC("ReceiveNewcomerNick", PhotonTargets.MasterClient, nickName, newcomer);
        }

        //CLIENT ONLY
        [PunRPC]
        public void ReceiveKickMessage()
        {
            Debug.Log("RECEBENDO Q FOI KIKADO...");
            GameObject.Find("KickPopup").GetComponent<OpenPopup>().openPopupElements(true);
            StartCoroutine(StartKickPlayer());
        }

        //SERVER ONLY
        [PunRPC]
        public void ReceiveNewcomerNick(string nickName, PhotonPlayer newcomer)
        {
            Debug.Log("VERIFICANDO NOVO NICK...");
            if(listNames.Contains(nickName))
            {
                Debug.Log("TEM UM NICK ASSIM!");
				FindObjectOfType<PhotonView>().RPC("ReceiveKickMessage", newcomer);
            }
        }

        IEnumerator StartKickPlayer()
        {
            yield return new WaitForSeconds(4f);
            PhotonNetwork.Disconnect();
        }

        //SERVER ONLY
        IEnumerator ServerSendPlayerNicks()
        {
            yield return new WaitForSeconds(2f);

            for (int i = 0; i < listNames.Count; i++)
            {
                if (listNames[i] == PhotonNetwork.playerName)
					FindObjectOfType<PhotonView>().RPC("ReceivePlayerToList", PhotonTargets.Others, listNames[i], true);
                else
					FindObjectOfType<PhotonView>().RPC("ReceivePlayerToList", PhotonTargets.Others, listNames[i], false);
            }

			FindObjectOfType<PhotonView>().RPC("RefreshLocalText", PhotonTargets.All);
        }

        void Update()
        {
            if (PhotonNetwork.room != null)
            {
                if (Application.loadedLevelName == "Rooms")
                {
                    if(!isLoaded)
                    {
                        GameObject.Find("Carregando").GetComponent<OpenPopup>().openPopupElements(false);
                        Debug.Log("FECHANDO CARREGANDO");
                        isLoaded = true;
                    }

                    if (btPlayGame == null)
                    {
                        btPlayGame = GameObject.Find("btPlay");
                    }

                    if (listNames.Count >= 3 && PhotonNetwork.isMasterClient)
                    {
						if(btPlayGame)
                   	 	   btPlayGame.SetActive(true);
                    }
                    else
                    {
						if(btPlayGame)
	                       btPlayGame.SetActive(false);
                    }

                    if (listNames.Count >= 3)
                    {
                        GameObject.Find("txWarning").GetComponent<Text>().text = "Aguardando dono da sala iniciar a partida...";
                    }
                    else
                    {
                        GameObject.Find("txWarning").GetComponent<Text>().text = "É necessário no mínimo 3 jogadores para iniciar...";
                    }
                }
            }
            else
            {
                isLoaded = false;
            }
        }

        public void StartGameScene()
        {
			ExitGames.Client.Photon.Hashtable currRoomProps = PhotonNetwork.room.customProperties;
			string passValue = (string)currRoomProps ["Password"];
			
			ExitGames.Client.Photon.Hashtable newProps = new ExitGames.Client.Photon.Hashtable ();
			newProps.Add ("Password", passValue);
			newProps.Add ("Playing", 1);
			
			PhotonNetwork.room.SetCustomProperties (newProps);

			FindObjectOfType<PhotonView>().RPC("SetGameManagerComp", PhotonTargets.All, true);
            GetComponent<NetworkLevelLoad>().SetLoadLevel("Game");
        }

        [PunRPC]
        public void SetGameManagerComp(bool isPlaying)
        {
            gameStatesClass.enabled = isPlaying;
        }
    }
}
