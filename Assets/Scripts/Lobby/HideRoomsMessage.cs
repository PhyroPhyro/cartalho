﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HideRoomsMessage : MonoBehaviour {

    void Update () {
	    if(GameObject.Find("Content").transform.childCount <= 0)
        {
            gameObject.GetComponent<Text>().enabled = true;
        }
        else
        {
            gameObject.GetComponent<Text>().enabled = false;
        }
	}
}