﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OpenPopup : MonoBehaviour {

    public List<GameObject> popupElements = new List<GameObject>();

	public void openPopupElements(bool isOpen)
    {
        Debug.Log("ABRINDO OU FECHANDO POPUP: " + isOpen);

        for (int i = 0; i < popupElements.Count; i++)
        {
            popupElements[i].SetActive(isOpen);
        }
    }

	public void closeElements()
	{
		Debug.Log("FECHANO POPUP");
		
		for (int i = 0; i < popupElements.Count; i++)
		{
			popupElements[i].SetActive(false);
		}
	}

	public void openElements()
	{
		Debug.Log("ABRINDO POPUP");
		
		for (int i = 0; i < popupElements.Count; i++)
		{
			popupElements[i].SetActive(true);
		}
	}
}
