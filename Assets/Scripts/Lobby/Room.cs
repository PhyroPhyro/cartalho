﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Google2u
{
    public class Room : MonoBehaviour
    {
        public RoomInfo roomData;

        public void OnClick()
        {
			ExitGames.Client.Photon.Hashtable roomProps = roomData.customProperties;
			int playingValue = (int)roomProps ["Playing"];

            if (playingValue == 0)
            {
                GameObject.FindObjectOfType<NetworkManager>().gameName = transform.FindChild("Text").GetComponent<Text>().text;


				string passValue = (string)roomProps["Password"];

                if(passValue.Length > 0)
                {
                    GameObject.Find("PasswordPopup").GetComponent<OpenPopup>().openPopupElements(true);
                    GameObject.Find("Correto").GetComponent<ConfirmPassword>().currRoomData = roomData;
                }
                else
                {
                    GameObject.Find("Carregando").GetComponent<OpenPopup>().openPopupElements(true);
                    Debug.Log("ABRINDO CARREGANDO");

					FindObjectOfType<RoomManager>().JoinServer(roomData);
                }

				Debug.Log("TEM SENHA: " + passValue + " lenght:" + passValue.Length);
            }
        }
    }
}