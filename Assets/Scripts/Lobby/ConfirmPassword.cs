﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Google2u
{
    public class ConfirmPassword : MonoBehaviour
    {
        public RoomInfo currRoomData;

        public void Confirm()
        {
            string password = GameObject.Find("InputField").GetComponent<InputField>().text;

            FindObjectOfType<RoomManager>().JoinServer(currRoomData, password);

            GameObject.Find("Carregando").GetComponent<OpenPopup>().openPopupElements(true);
        }
    }
}