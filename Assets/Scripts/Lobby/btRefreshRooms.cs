﻿using UnityEngine;
using System.Collections;

namespace Google2u
{
    public class btRefreshRooms : MonoBehaviour
    {

        public void OnClick()
        {
			PhotonNetwork.JoinLobby ();
            FindObjectOfType<RoomManager>().RefreshHostList();
        }
    }
}