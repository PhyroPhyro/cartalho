﻿using UnityEngine;
using System.Collections;

public class HidePasswordPopup : MonoBehaviour {

	public void ShowPop()
	{
		GameObject.Find("PasswordPanel").GetComponent<OpenPopup>().openPopupElements(true);
	}

	public void HidePop()
	{
		GameObject.Find("PasswordPanel").GetComponent<OpenPopup>().openPopupElements(false);
	}
}
