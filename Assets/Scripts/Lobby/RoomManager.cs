﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Google2u
{
    public class RoomManager : MonoBehaviour
    {
        private const string typeName = "CardsAgainst";
        public bool isRefreshingHostList = false;
        public RoomInfo[] hostList;
        public GameObject roomObj;

        public GameObject roomsCanvas;
        public GameObject lobbyCanvas;

        void Awake()
        {
			StartCoroutine (StartRefreshLobby ());
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
		}

		IEnumerator StartRefreshLobby()
		{
			yield return new WaitForSeconds (0.2f);
			Debug.Log ("REFRESHING LOBBY ENTER");
			PhotonNetwork.JoinLobby ();
		}

		void OnJoinedLobby()
		{
			RefreshHostList ();
			PhotonNetwork.autoJoinLobby = true;
		}
		
		void Update()
        {
            if (PhotonNetwork.room == null)
            {
                roomsCanvas.SetActive(true);
                lobbyCanvas.SetActive(false);


				hostList = PhotonNetwork.GetRoomList ();
				
                if (isRefreshingHostList)
                {
                    isRefreshingHostList = false;
					/*hostList = null;
                    hostList = MasterServer.PollHostList();
					Debug.Log("HOST LIST COUNT " +  MasterServer.PollHostList().Length);*/
                    RefreshRoomsObj();
                }
            }
            else
            {
                roomsCanvas.SetActive(false);
                lobbyCanvas.SetActive(true);
            }
        }

        private void RefreshRoomsObj()
        {
			hostList = PhotonNetwork.GetRoomList ();

            for(int i = 0; i < hostList.Length; i++)
            {
				Debug.Log("CREATING ROOM IN LIST");

                GameObject currRoom = GameObject.Instantiate(roomObj);
                currRoom.transform.SetParent(GameObject.Find("Content").transform, false);
                //currRoom.transform.parent = GameObject.Find("Content").transform;
                currRoom.transform.FindChild("Text").GetComponent<Text>().text = hostList[i].name;
                currRoom.transform.FindChild("Players").GetComponent<Text>().text = hostList[i].playerCount + "/5";
                currRoom.GetComponent<Room>().roomData = hostList[i];

				ExitGames.Client.Photon.Hashtable roomAttrib = hostList[i].customProperties;
				string passvalue = (string)roomAttrib["Password"];
				int playingValue = (int)roomAttrib["Playing"];

                if (passvalue.Length <= 0)
                    currRoom.transform.FindChild("Locked").GetComponent<Image>().enabled = false;

				if (playingValue == 0)
                    currRoom.transform.FindChild("Status").GetComponent<Image>().color = new Color(0, 255, 0, 255);
                else
					currRoom.transform.FindChild("Status").GetComponent<Image>().color = new Color(255, 0, 0, 255);
        }
            Debug.Log("DONE REFRESHING ROOMS LIST");
        }

        public void RefreshHostList()
        {
            Debug.Log("ASKING FOR SERVER LIST");

			foreach (Transform child in GameObject.Find("Content").transform) {
				Debug.Log("DESTROY");
				GameObject.Destroy(child.gameObject);
			}

            //MasterServer.ClearHostList();
            isRefreshingHostList = true;
            //MasterServer.RequestHostList(typeName);
        }

        public void JoinServer(RoomInfo roomInfo, string currPassword = null)
        {
			if (currPassword != null) {
				ExitGames.Client.Photon.Hashtable roomProps = roomInfo.customProperties;
				string passvalue = (string)roomProps ["Password"];
				if (currPassword == passvalue){
					PhotonNetwork.JoinRoom (roomInfo.name);
				}else{
					GameObject.Find("WrongPassPopup").GetComponent<OpenPopup>().openPopupElements(true);
					GameObject.Find("Carregando").GetComponent<OpenPopup>().closeElements();
				}
			} else {
				PhotonNetwork.JoinRoom (roomInfo.name);
			}
        }
    }
}