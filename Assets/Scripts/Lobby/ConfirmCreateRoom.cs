﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Google2u
{
    public class ConfirmCreateRoom : MonoBehaviour
    {

        public void OnClick()
        {
            GameObject.FindObjectOfType<NetworkManager>().gameName = GameObject.Find("NomeInput").GetComponent<InputField>().text;
           
            if (GameObject.Find("SenhaInput") != null)
              GameObject.FindObjectOfType<NetworkManager>().gamePass = GameObject.Find("SenhaInput").GetComponent<InputField>().text;

            GameObject.FindObjectOfType<NetworkManager>().StartServer();
        }
    }
}