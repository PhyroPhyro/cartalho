﻿using UnityEngine;
using System.Collections;

public class TogglePrivate : MonoBehaviour {

    public GameObject toggleText;
    public GameObject senhaField;

	public void OnToggle()
    {
        toggleText.SetActive(!toggleText.activeSelf);
        senhaField.SetActive(!senhaField.activeSelf);
    }
}
