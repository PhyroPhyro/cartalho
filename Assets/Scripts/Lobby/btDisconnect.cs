﻿using UnityEngine;
using System.Collections;

public class btDisconnect : MonoBehaviour {

	public void OnClick()
    {
        PhotonNetwork.LeaveRoom();
		PhotonNetwork.JoinLobby ();
    }
}
