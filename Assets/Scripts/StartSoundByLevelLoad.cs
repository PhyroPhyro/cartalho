﻿using UnityEngine;
using System.Collections;

public class StartSoundByLevelLoad : MonoBehaviour {

    void OnLevelWasLoaded(int level)
    {
        if(Application.loadedLevelName == "Main")
        {
            GetComponent<AudioSource>().Play();
        }
    }
}
