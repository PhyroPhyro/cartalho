using UnityEngine;
using System.Collections;

namespace Google2u
{
    public class NetworkLevelLoad : MonoBehaviour
    {

        public int lastLevelPrefix = 0;

        public void SetLoadLevel(string level)
        {
            FindObjectOfType<PhotonView>().RPC("LoadLevel", PhotonTargets.AllBuffered, level, lastLevelPrefix + 1);
        }

        [PunRPC]
        IEnumerator LoadLevel(string level, int levelPrefix)
        {

            lastLevelPrefix = levelPrefix;

            // There is no reason to send any more data over the network on the default channel,
            // because we are about to load the level, thus all those objects will get deleted anyway
          //  Network.SetSendingEnabled(0, false);

            // We need to stop receiving because first the level must be loaded first.
            // Once the level is loaded, rpc's and other state update attached to objects in the level are allowed to fire
           // Network.isMessageQueueRunning = false;

            // All network views loaded from a level will get a prefix into their NetworkViewID.
            // This will prevent old updates from clients leaking into a newly created scene.
            Application.LoadLevel(level);
			yield return new WaitForSeconds(0);

            // Allow receiving data again
            // Now the level has been loaded and we can start sending out data to clients
        }
    }
}