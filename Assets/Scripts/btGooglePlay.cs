﻿using UnityEngine;
using System.Collections;

public class btGooglePlay : MonoBehaviour {

	public void OnClick()
	{
		FindObjectOfType<AndroidServicesManager> ().GooglePlayLogin ();
	}

	void Update()
	{
		if (FindObjectOfType<AndroidServicesManager> ().GooglePlayCheck ())
			gameObject.SetActive (false);
	}
}